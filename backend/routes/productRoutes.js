const express = require("express");
const router = express.Router();

const { getProducts, getProduct, getMultipleProducts } = require('../controller/productController')
const { protect } = require('../middleware/auth')

router.get('/', protect, getProducts)
router.get('/by-ids', protect, getMultipleProducts)
router.get('/:productId', protect, getProduct)

module.exports = router;
