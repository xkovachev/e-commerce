export const DUMMY_PRODUCTS = [
  {
    type: 'Headphones',
    images: [
      'https://www.grundig-gbs.com/fileadmin/images/Schreibplaetze/Zubehoer/Digital-dictation_Digta-Headphone-565_GBS.png',
      'https://images.ctfassets.net/8cd2csgvqd3m/64gMlG7afaGf8K1RYlcUvF/4740333b31aca9f4de094ce85220ee1d/Hx_Black_3.png',
      'https://media.cntraveler.com/photos/5ea1b20f9fc81500085a4f10/master/w_1809,h_1206,c_limit/8-headphones.jpg',
    ],
  },
  {
    type: 'Watches',
    images: [
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F13.FossilWatchBrown.png&w=1920&q=75',
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F14.MVMTMWatchBlack.png&w=1920&q=75',
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F15.BarihoWatchBlack.png&w=1920&q=75',
    ],
  },
  {
    type: 'Sunglasses',
    images: [
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F10.RayBanOcean.png&w=1920&q=75',
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F8.RayBanMattBlack.png&w=1920&q=75',
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F9.RayBanBlack.png&w=1920&q=75',
    ],
  },
  {
    type: 'Jewelry',
    images: [
      'https://www.grundig-gbs.com/fileadmin/images/Schreibplaetze/Zubehoer/Digital-dictation_Digta-Headphone-565_GBS.png',
      'https://images.ctfassets.net/8cd2csgvqd3m/64gMlG7afaGf8K1RYlcUvF/4740333b31aca9f4de094ce85220ee1d/Hx_Black_3.png',
      'https://media.cntraveler.com/photos/5ea1b20f9fc81500085a4f10/master/w_1809,h_1206,c_limit/8-headphones.jpg',
    ],
  },
  {
    type: 'Furniture',
    images: [
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F13.FossilWatchBrown.png&w=1920&q=75',
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F14.MVMTMWatchBlack.png&w=1920&q=75',
      'https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F15.BarihoWatchBlack.png&w=1920&q=75',
    ],
  },
]
