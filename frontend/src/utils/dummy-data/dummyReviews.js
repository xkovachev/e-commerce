export const DUMMY_REVIEWS = [
  {
    author: 'Jannie Schumm',
    image: 'https://bazaar.ui-lib.com/assets/images/faces/7.png',
    rating: 5.0,
    when: '1.8 years ago',
    comment:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius massa id ut mattis. Facilisis vitae gravida egestas ac account.',
  },
  {
    author: 'Joe Kenan',
    image: 'https://bazaar.ui-lib.com/assets/images/faces/6.png',
    rating: 3.1,
    when: '2.4 years ago',
    comment:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius massa id ut mattis. Facilisis vitae gravida egestas ac account.',
  },
  {
    author: 'Jenifer Tulio',
    image: 'https://bazaar.ui-lib.com/assets/images/faces/8.png',
    rating: 4.2,
    when: '3.3 years ago',
    comment:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius massa id ut mattis. Facilisis vitae gravida egestas ac account.',
  },
]
