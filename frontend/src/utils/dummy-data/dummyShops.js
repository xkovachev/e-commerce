export const DUMMY_SHOPS = [
  {
    name: 'Billa',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Billa-Logo.svg/2560px-Billa-Logo.svg.png',
  },
  {
    name: 'Kaufland',
    image:
      'https://www.kaufland.bg/etc.clientlibs/kaufland/clientlibs/clientlib-klsite/resources/frontend/img/opengraph_image_default-a2531d9dba.png',
  },
  {
    name: 'LIDL',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Lidl-Logo.svg/1024px-Lidl-Logo.svg.png',
  },
  {
    name: 'Metro',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Logo_METRO.svg/2560px-Logo_METRO.svg.png',
  },
  {
    name: 'T-Market',
    image: 'https://eu.leafletscdns.com/bg/data/5/logo.png',
  },
]
