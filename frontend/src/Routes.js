import Loading from 'views/Loading'
import { Suspense, lazy } from 'react'
import { Route, Routes } from 'react-router-dom'
import { useAuth } from 'core'
import { Main as MainLayout, Minimal as MinimalLayout } from 'layouts'
import { PrivateRoute, PublicRoute, AdminRoute } from 'components'

const HomeView = lazy(() => import('views/Home'))
const CartView = lazy(() => import('views/Cart'))
const LoginView = lazy(() => import('views/Login'))
const NotFoundView = lazy(() => import('views/NotFound'))
const NoPermissionView = lazy(() => import('views/NoPermission'))
const OrderView = lazy(() => import('views/OrderDetails'))
const RegisterView = lazy(() => import('views/Register'))
const ProductDetailsView = lazy(() => import('views/ProductDetails'))
const CheckoutView = lazy(() => import('views/Checkout'))
const AllOrdersView = lazy(() => import('views/AllOrders'))

export const PageURLs = {
  Cart: '/cart',
  Login: '/login',
  NotFound: '/404',
  NoPermission: '/no-permission',
  Register: '/register',
  ProductDetails: '/products/:productId',
  Order: '/order',
  Checkout: '/checkout',
  Admin: '/admin',
}

const RoutesComponent = () => {
  const { isAuthenticated, loadingAuth } = useAuth()

  return !loadingAuth ? (
    <Suspense
      fallback={
        isAuthenticated ? (
          <MainLayout isSuspense={true} />
        ) : (
          <MinimalLayout isSuspense={true}>
            <Loading />
          </MinimalLayout>
        )
      }
    >
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route
            index
            element={
              <PrivateRoute>
                <HomeView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Cart}
            element={
              <PrivateRoute>
                <CartView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.ProductDetails}
            element={
              <PrivateRoute>
                <ProductDetailsView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Checkout}
            element={
              <PrivateRoute>
                <CheckoutView />
              </PrivateRoute>
            }
          />
          <Route
            path={`${PageURLs.Order}/:orderId`}
            element={
              <PrivateRoute>
                <OrderView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Admin}
            element={
              <AdminRoute>
                <AllOrdersView />
              </AdminRoute>
            }
          />
          <Route
            path={PageURLs.NoPermission}
            element={
                <NoPermissionView />
            }
          />
          <Route path="*" element={<NotFoundView />} />
        </Route>
        <Route path="/" element={<MinimalLayout />}>
          <Route
            path={PageURLs.Login}
            element={
              <PublicRoute>
                <LoginView />
              </PublicRoute>
            }
          />
          <Route
            path={PageURLs.Register}
            element={
              <PublicRoute>
                <RegisterView />
              </PublicRoute>
            }
          />
        </Route>
      </Routes>
    </Suspense>
  ) : (
    ''
  )
}

export default RoutesComponent
