import { Mail, Phone } from '@mui/icons-material'
import { Box, CircularProgress, Container, Typography } from '@mui/material'
import { ErrorBoundary } from 'components'
import { Outlet } from 'react-router-dom/dist'
import { NavigationBar, Footer, MenusBar } from './components'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import theme from '../../theme'

const Main = ({ isSuspense }) => {
  return (
    <ErrorBoundary sx={{ minHeight: '100vh', display: 'flex', flexDirection: 'column' }}>
      <Box
        sx={{
          height: 40,
          bgcolor: 'primary.main',
          display: { xs: 'none', md: 'flex' },
          alignItems: 'center',
        }}
      >
        <Container
          maxWidth={1280}
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', maxWidth: 1280 }}
        >
          <Box display="flex" gap={1}>
            <Typography
              component="div"
              sx={{ display: 'flex', alignItems: 'center', mr: 2, fontSize: 12 }}
              color="background.default"
            >
              <Phone sx={{ fontSize: 20 }} />
              &nbsp; &nbsp;+88012 3456 7894
            </Typography>
            <Typography
              component="div"
              sx={{ display: 'flex', alignItems: 'center', fontSize: 12 }}
              color="background.default"
            >
              <Mail sx={{ mr: 1, fontSize: 19 }} />
              support@ui-lib.com
            </Typography>
          </Box>
          <Box>
            <Box sx={{ display: 'flex', alignItems: 'center', fontSize: 12, gap: 4 }} component="div">
              <Typography
                color="background.default"
                sx={{
                  fontSize: 12,
                  ':hover': { cursor: 'pointer', color: theme.palette.secondary.alternate },
                }}
              >
                Theme FAQ"s
              </Typography>
              <Typography
                color="background.default"
                sx={{
                  fontSize: 12,
                  ':hover': { cursor: 'pointer', color: theme.palette.secondary.alternate },
                }}
              >
                Need Help?
              </Typography>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  ':hover': {
                    cursor: 'pointer',
                  },
                }}
              >
                <Typography
                  color="background.default"
                  sx={{
                    fontWeight: 600,
                    fontSize: 12,
                  }}
                >
                  EN
                </Typography>
                <KeyboardArrowDownIcon sx={{ fontSize: 15, color: 'white' }} />
              </Box>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  ':hover': {
                    cursor: 'pointer',
                  },
                }}
              >
                <Typography
                  color="background.default"
                  sx={{
                    fontWeight: 600,
                    fontSize: 11.5,
                  }}
                >
                  USD
                </Typography>
                <KeyboardArrowDownIcon sx={{ fontSize: 15, color: 'white' }} />
              </Box>
            </Box>
          </Box>
        </Container>
      </Box>
      <NavigationBar />
      <MenusBar />
      {isSuspense ? (
        <>
          <CircularProgress />
          <Footer />
        </>
      ) : (
        <>
          <Outlet />
          <Footer />
        </>
      )}
    </ErrorBoundary>
  )
}

export default Main
