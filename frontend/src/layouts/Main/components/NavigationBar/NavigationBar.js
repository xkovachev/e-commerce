import {
  Badge,
  ButtonBase,
  AppBar,
  Box,
  Toolbar,
  IconButton,
  Typography,
  Menu,
  Container,
  Avatar,
  Tooltip,
  MenuItem,
  InputBase,
} from '@mui/material'
import { useState, Fragment } from 'react'
import { useAuth } from 'core'
import { Link, useNavigate } from 'react-router-dom'
import { CartButton } from './components'
import SearchIcon from '@mui/icons-material/Search'
import theme from '../../../../theme'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import { useCart } from 'core'

const NavigationBar = () => {
  const { user, logout, isAdmin } = useAuth()
  const { quantity } = useCart()
  const [anchorElUser, setAnchorElUser] = useState(null)
  const [searchMenuIsOpen, setSearchMenuIsOpen] = useState(null)

  const navigate = useNavigate()

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget)
  }

  const handleCloseUserMenu = () => {
    setAnchorElUser(null)
  }

  const handleOpenSearchMenu = (event) => {
    setSearchMenuIsOpen(event.currentTarget)
  }

  const handleCloseSearchMenu = () => {
    setSearchMenuIsOpen(null)
  }

  const adminPanelRedirect = () => {
    handleCloseUserMenu()
    navigate('/admin')
  }

  return (
    <Fragment>
      <AppBar
        position="sticky"
        sx={{
          bgcolor: 'background.default',
          borderColor: (theme) => theme.palette.grey[200],
        }}
        elevation={0}
      >
        <Container maxWidth={1280} sx={{ maxWidth: 1280, maxHeight: 80, height: 80 }}>
          <Toolbar disableGutters sx={{ display: 'flex', justifyContent: 'space-between', height: '100%' }}>
            <Box sx={{ maxWidth: 170, width: '100%', height: '100%', display: 'flex', alignItems: 'center', mt: 0.9 }}>
              <Link to="/">
                <Box
                  component="img"
                  src="https://bazaar.ui-lib.com/assets/images/logo2.svg"
                  alt="logo"
                  maxHeight={44}
                />
              </Link>
            </Box>
            <Box
              sx={{
                border: `1px solid ${theme.palette.grey[400]}`,
                borderRadius: 10,
                maxWidth: 670,
                width: '100%',
                height: 44,
                display: 'flex',
                ml: 2.5,
                '&:hover': {
                  border: `1px solid ${theme.palette.secondary.alternate}`,
                },
              }}
            >
              <IconButton sx={{ pointerEvents: 'none' }}>
                <SearchIcon sx={{ fontSize: '1.25rem', ml: 0.5, color: theme.palette.grey[500] }} />
              </IconButton>
              <InputBase
                component="input"
                placeholder={'Searching for...'}
                sx={{ maxWidth: 470, width: '100%', fontSize: 14 }}
              />
              <Box
                sx={{
                  height: '100%',
                  maxWidth: 255,
                  mb: 2,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <ButtonBase
                  sx={{
                    borderLeft: `1px solid ${theme.palette.grey[400]}`,
                    height: '100%',
                    maxWidth: 260,
                    width: 160,
                    borderRadius: '0 20px 20px 0',
                    bgcolor: theme.palette.grey[200],
                  }}
                  onClick={handleOpenSearchMenu}
                >
                  <Typography sx={{ color: 'gray', textAlign: 'center', fontSize: 14 }}>
                    All Categories&nbsp;
                  </Typography>
                  <KeyboardArrowDownIcon sx={{ fontSize: 19, color: 'gray' }} />
                </ButtonBase>
                <Menu
                  sx={{ mt: 5.4 }}
                  id="search-menu"
                  anchorEl={searchMenuIsOpen}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={Boolean(searchMenuIsOpen)}
                  onClick={handleCloseSearchMenu}
                  onClose={handleCloseSearchMenu}
                >
                  <MenuItem>
                    <Typography textAlign="center">All categories</Typography>
                  </MenuItem>
                  <MenuItem>
                    <Typography textAlign="center">Cars</Typography>
                  </MenuItem>
                  <MenuItem>
                    <Typography textAlign="center">Clothes</Typography>
                  </MenuItem>
                  <MenuItem>
                    <Typography textAlign="center">Electronics</Typography>
                  </MenuItem>
                </Menu>
              </Box>
            </Box>
            <Box display="flex" alignItems="center">
              <Tooltip title="Open settings">
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0, mr: 2 }}>
                  <Avatar alt={user?.name}>{user?.name[0]}</Avatar>
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClick={handleCloseUserMenu}
              >
                {isAdmin && (
                  <MenuItem onClick={adminPanelRedirect}>
                    <Typography textAlign="center">Admin Panel</Typography>
                  </MenuItem>
                )}
                <MenuItem onClick={logout}>
                  <Typography textAlign="center">Logout</Typography>
                </MenuItem>
              </Menu>
              <CartButton />
              <Badge badgeContent={quantity} color="error" sx={{ position: 'absolute', right: 20, top: 25 }} />
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </Fragment>
  )
}
export default NavigationBar
