import { Close, ShoppingBagOutlined } from '@mui/icons-material'
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardMedia,
  Divider,
  Drawer,
  IconButton,
  Typography,
  ButtonBase,
  Tooltip,
} from '@mui/material'
import { DisplayCurrency } from 'components'
import { useCart } from 'core'
import { Fragment, useState } from 'react'

import { useNavigate } from 'react-router-dom'
import { PageURLs } from 'Routes'
import theme from 'theme'
import { Link } from 'react-router-dom'

const CartButton = () => {
  const { cart, removeFromCart, addToCart, getFinalPrice, totalPrice, quantity, removeWholeProductFromCart } = useCart()
  const navigate = useNavigate()

  const [state, setState] = useState(false)

  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return
    }

    setState(open)
  }
  return (
    <div>
      <IconButton sx={{ p: 0, mr: 2 }} onClick={toggleDrawer(true)}>
        <Avatar>
          <ShoppingBagOutlined />
        </Avatar>
      </IconButton>

      <Drawer
        anchor="right"
        open={state}
        onClose={toggleDrawer(false)}
        PaperProps={{
          sx: { display: 'flex', flexDirection: 'column', justifyContent: 'space-between' },
        }}
      >
        <IconButton size="small" sx={{ position: 'absolute', top: 8, right: 8 }} onClick={toggleDrawer(false)}>
          <Close />
        </IconButton>
        <Box>
          <Box display="flex" sx={{ height: 74 }} alignItems="center" p={2}>
            <ShoppingBagOutlined sx={{ color: theme.palette.primary.main }} />{' '}
            <Typography variant="body1" color={theme.palette.primary.main} sx={{ ml: 1 }}>
              {quantity} items
            </Typography>
          </Box>
          <Divider />
          {cart.map((product) => {
            const finalPrice = getFinalPrice(product)
            return (
              <Fragment key={product._id}>
                <Card
                  sx={{ p: 2, display: 'flex', height: 125, alignItems: 'center', position: 'relative' }}
                  elevation={0}
                >
                  <Box display="flex" flexDirection="column" alignItems="center" sx={{ gap: 1 }}>
                    <ButtonBase
                      sx={{
                        color: theme.palette.secondary.alternate,
                        border: `1px solid ${theme.palette.secondary.alternate}`,
                        borderRadius: '50%',
                        width: 32,
                        height: 32,
                        fontSize: 20,
                      }}
                      onClick={() => addToCart(product)}
                    >
                      +
                    </ButtonBase>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                      <Typography textAlign="center" fontWeight={600} fontSize={14}>
                        {cart.map((item) => {
                          if (item._id === product._id) {
                            return item.quantity
                          }
                        })}
                      </Typography>
                    </Box>
                    <ButtonBase
                      sx={{
                        color: theme.palette.secondary.alternate,
                        border: `1px solid ${theme.palette.secondary.alternate}`,
                        borderRadius: '50%',
                        width: 32,
                        height: 32,
                        fontSize: 20,
                      }}
                      onClick={() => removeFromCart(product._id)}
                    >
                      -
                    </ButtonBase>
                  </Box>
                  <Box>
                    <Link to={`/products/${product._id}`}>
                      <CardMedia
                        component="img"
                        sx={{ width: 76, height: 76, m: 1 }}
                        image={product.thumbnail}
                        alt={product.title}
                      />
                    </Link>
                  </Box>
                  <CardContent
                    sx={{
                      flex: '1 0 auto',
                      height: 76,
                      p: 0,
                      pl: 2,
                      pb: '0 !important',
                    }}
                  >
                    <Box display="flex" justifyContent="space-between">
                      <Box maxWidth="140px">
                        <Tooltip title={product.title}>
                          <Typography noWrap fontSize={15}>
                            {product.title}
                          </Typography>
                        </Tooltip>
                        <Box display="flex" flexDirection="column" alignItems="flex-start">
                          <Box display="flex" flexDirection="row" sx={{ color: theme.palette.grey[500], fontSize: 11 }}>
                            <DisplayCurrency number={finalPrice} />
                            <Typography fontSize={11}>&nbsp;x&nbsp;</Typography>
                            <Typography fontSize={11}>{product.quantity}</Typography>
                          </Box>
                          <Box sx={{ display: 'flex', color: 'secondary.alternate', display: 'inline' }}>
                            <Typography fontSize={14} fontWeight={600}>
                              <DisplayCurrency number={finalPrice * product.quantity} />
                            </Typography>
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  </CardContent>
                  <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <IconButton onClick={() => removeWholeProductFromCart(product._id)}>
                      <Close sx={{ fontSize: 17 }} />
                    </IconButton>
                  </Box>
                </Card>
                <Divider />
              </Fragment>
            )
          })}
        </Box>
        <Box p={2}>
          <Button
            variant="contained"
            fullWidth
            sx={{
              mb: 1,
              bgcolor: theme.palette.secondary.alternate,
              ':hover': {
                bgcolor: '#dc324e',
              },
            }}
          >
            Checkout Now (<DisplayCurrency number={totalPrice} />)
          </Button>
          <Button
            sx={{
              border: `0.5px solid ${theme.palette.secondary.alternate}`,
              color: theme.palette.secondary.alternate,
              ':hover': {
                border: `0.5px solid ${theme.palette.secondary.alternate}`,
                bgcolor: 'white',
              },
            }}
            variant="outlined"
            fullWidth
            onClick={() => {
              navigate(PageURLs.Checkout)
              toggleDrawer()(false)
            }}
          >
            View Cart
          </Button>
        </Box>
      </Drawer>
    </div>
  )
}

export default CartButton
