import { Box, CircularProgress, Container, Divider, Typography } from '@mui/material'

const PageLayout = ({
  error,
  children,
  data,
  isAsync = true,
  withContainer = true,
  title,
  loading = <CircularProgress />,
}) => {
  if (error)
    return (
      <Container>
        <Typography>Oopsie, we had an error, please contact the system administrator</Typography>
      </Container>
    )

  if (!data && !error && isAsync) return <Container>{loading}</Container>

  return withContainer ? (
    <Container disableGutters sx={{ py: 4 }}>
      {children}
    </Container>
  ) : (
    children
  )
}

export default PageLayout
