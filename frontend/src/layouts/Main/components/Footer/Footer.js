import { Container, Box, Typography, Grid, IconButton } from '@mui/material'
import { Link } from 'react-router-dom'
import FacebookIcon from '@mui/icons-material/Facebook'
import TwitterIcon from '@mui/icons-material/Twitter'
import YouTubeIcon from '@mui/icons-material/YouTube'
import GoogleIcon from '@mui/icons-material/Google'
import InstagramIcon from '@mui/icons-material/Instagram'
import theme from 'theme'

const Footer = () => {
  const icons = [
    <FacebookIcon sx={{ color: 'white', fontSize: 18 }} />,
    <TwitterIcon sx={{ color: 'white', fontSize: 18 }} />,
    <YouTubeIcon sx={{ color: 'white', fontSize: 18 }} />,
    <GoogleIcon sx={{ color: 'white', fontSize: 18 }} />,
    <InstagramIcon sx={{ color: 'white', fontSize: 18 }} />,
  ]

  return (
    <Box sx={{ bgcolor: '#222935' }}>
      <Container
        maxWidth="xl"
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
          mt: 'auto',
        }}
      >
        <Grid
          container
          disableGutters
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            my: 12,
            mx: 15,
          }}
        >
          <Grid
            item
            sx={{
              color: 'white',
              maxWidth: 418,
              display: 'grid',
              gap: 2,
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              flexDirection: 'column',
            }}
          >
            <Link to="/">
              <Box
                component="img"
                src="https://bazaar.ui-lib.com/assets/images/logo.svg"
                alt="logo"
                sx={{ color: 'white' }}
              />
            </Link>
            <Box sx={{ color: theme.palette.grey[500], display: 'grid', gap: 1 }}>
              <Typography fontSize={15}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor libero id et, in gravida. Sit diam duis
                mauris nulla cursus. Erat et lectus vel ut sollicitudin elit at amet.
              </Typography>
            </Box>
            <Box>
              <Box
                component="img"
                alt="Google Play logo"
                src="https://lh3.googleusercontent.com/q1k2l5CwMV31JdDXcpN4Ey7O43PxnjAuZBTmcHEwQxVuv_2wCE2gAAQMWxwNUC2FYEOnYgFPOpw6kmHJWuEGeIBLTj9CuxcOEeU8UXyzWJq4NJM3lg=s0"
                sx={{ maxWidth: 145, maxHeight: 49, mr: 1 }}
              />
              <Box
                component="img"
                alt="App Store logo"
                src="https://upload.wikimedia.org/wikipedia/commons/5/5d/Available_on_the_App_Store_%28black%29.png"
                sx={{ maxWidth: 145, maxHeight: 49 }}
              />
            </Box>
          </Grid>

          <Grid item sx={{ color: 'white', maxWidth: 210 }}>
            <Box sx={{ mb: 2 }}>
              <Typography variant="h6" fontWeight={600}>
                About Us
              </Typography>
            </Box>
            <Box sx={{ color: theme.palette.grey[500], display: 'grid', gap: 1 }}>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Careers</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Our Stores</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Our Cares</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Terms & Conditions</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Privacy Policy</Typography>
            </Box>
          </Grid>

          <Grid item sx={{ color: 'white', maxWidth: 315 }}>
            <Box sx={{ mb: 2 }}>
              <Typography variant="h6" fontWeight={600}>
                Customer Care
              </Typography>
            </Box>
            <Box
              sx={{
                color: theme.palette.grey[500],
                display: 'grid',
                gap: 1,
              }}
            >
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Help Center</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>How to Buy</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Track Your Order</Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>
                Corporate & Bulk Purchasing
              </Typography>
              <Typography sx={{ ':hover': { cursor: 'pointer', color: 'white' } }}>Returns & Refunds</Typography>
            </Box>
          </Grid>

          <Grid item disableGutters sx={{ color: 'white', maxWidth: 315 }}>
            <Box sx={{ display: 'grid', gap: 2 }}>
              <Box>
                <Typography variant="h6" fontWeight={600}>
                  Contact Us
                </Typography>
              </Box>
              <Box sx={{ color: theme.palette.grey[500], display: 'grid', gap: 1 }}>
                <Typography>70 Washington Square South, New York, NY 10012, United States</Typography>
                <Typography>Email: uilib.help@gmail.com</Typography>
                <Typography>Phone: +1 1123 456 780</Typography>
                <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                  {icons.map((icon, index) => (
                    <Box
                      key={`icon${index}`}
                      sx={{
                        backgroundColor: '#1b212a',
                        borderRadius: '50%',
                        width: 30,
                        heigth: 30,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        mr: 1,
                        ':hover': {
                          backgroundColor: '#232d3b',
                        },
                      }}
                    >
                      <IconButton>{icon}</IconButton>
                    </Box>
                  ))}
                </Box>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}

export default Footer
