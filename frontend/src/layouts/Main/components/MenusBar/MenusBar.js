import { Menu, ButtonBase, Box, Typography, MenuItem, NestedMenuItem } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import WidgetsIcon from '@mui/icons-material/Widgets'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
// import NestedMenuItem from 'material-ui-nested-menu-item'
import theme from 'theme'
import { useState } from 'react'
import useSWR from 'swr'

const MenusBar = (props) => {
  const [categoryMenuIsOpen, setCategoryMenuIsOpen] = useState(null)
  const { data: categories, error } = useSWR('/categories')

  const handleOpenCategoryMenu = (event) => {
    setCategoryMenuIsOpen(event.currentTarget)
  }

  const handleCloseCategoryMenu = () => {
    setCategoryMenuIsOpen(null)
  }

  return (
    <Box
      sx={{
        height: 60,
        bgcolor: 'background.default',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottom: `1px solid ${theme.palette.grey[200]}`,
      }}
    >
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          maxWidth: 1280,
          width: '100%',
          px: 3,
        }}
      >
        <Box sx={{ bgcolor: theme.palette.grey[200], maxWidth: 278, maxHeight: 40, width: '100%', height: 40 }}>
          <ButtonBase onClick={handleOpenCategoryMenu} sx={{ display: 'flex', justifyContent: 'space-between', width: '100%', height: '100%' }}>
            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <WidgetsIcon sx={{ fontSize: 20, m: 1 }} />
              <Typography sx={{ fontSize: 14, fontWeight: 'bold', color: theme.palette.grey[600] }}>
                Categories
              </Typography>
            </Box>
            <KeyboardArrowRightIcon sx={{ fontSize: 20, m: 1 }} />
          </ButtonBase>
          <Menu
            sx={{ mt: 5, ml: 15.95 }}
            id="category-menu"
            anchorEl={categoryMenuIsOpen}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={Boolean(categoryMenuIsOpen)}
            onClose={handleCloseCategoryMenu}
          >
            {categories?.result?.map(category => {
              return (
                <MenuItem key={category._id}>
                  <Typography textAlign="center">{category.name}</Typography>
                </MenuItem>
              )
            })}
          </Menu>
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            gap: 4.1,
            fontSize: 14,
          }}
        >
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              '&:hover': {
                color: theme.palette.secondary.alternate,
                cursor: 'pointer',
              },
            }}
          >
            Home&nbsp;
            <KeyboardArrowDownIcon sx={{ color: theme.palette.grey[500], fontSize: 16 }} />
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              '&:hover': {
                color: theme.palette.secondary.alternate,
                cursor: 'pointer',
              },
            }}
          >
            Mega Menu&nbsp;
            <KeyboardArrowDownIcon sx={{ color: theme.palette.grey[500], fontSize: 16 }} />
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              '&:hover': {
                color: theme.palette.secondary.alternate,
                cursor: 'pointer',
              },
            }}
          >
            Full Screen Menu&nbsp;
            <KeyboardArrowDownIcon sx={{ color: theme.palette.grey[500], fontSize: 16 }} />
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              '&:hover': {
                color: theme.palette.secondary.alternate,
                cursor: 'pointer',
              },
            }}
          >
            Pages&nbsp;
            <KeyboardArrowDownIcon sx={{ color: theme.palette.grey[500], fontSize: 16 }} />
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              '&:hover': {
                color: theme.palette.secondary.alternate,
                cursor: 'pointer',
              },
            }}
          >
            User Account&nbsp;
            <KeyboardArrowDownIcon sx={{ color: theme.palette.grey[500], fontSize: 16 }} />
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              '&:hover': {
                color: theme.palette.secondary.alternate,
                cursor: 'pointer',
              },
            }}
          >
            Vendor Account&nbsp;
            <KeyboardArrowDownIcon sx={{ color: theme.palette.grey[500], fontSize: 16 }} />
          </Box>
        </Box>
      </Box >
    </Box >
  )
}

export default MenusBar
