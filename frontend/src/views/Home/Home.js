import {
  ProductList,
  ShoeSwiper,
  FlashDealsSwiper,
  TopCategoriesSwiper,
  TopRatedFeaturedBrands,
  NewArrivals,
  BigDiscounts,
  CategoriesSection,
  Electronics,
  Beauty,
  MiscProductsSection,
  ShopTraits,
  CustomSettingsIcon,
} from './components'

const Home = () => {
  return (
    <>
      <ShoeSwiper />
      <FlashDealsSwiper />
      <TopCategoriesSwiper />
      <TopRatedFeaturedBrands />
      <NewArrivals />
      <BigDiscounts />
      <MiscProductsSection />
      <Electronics />
      <Beauty />
      <CategoriesSection />
      <ProductList />
      <ShopTraits />
      <CustomSettingsIcon />
    </>
  )
}

export default Home
