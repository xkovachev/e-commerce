import { Box, Grid, Pagination, TextField, Typography, MenuItem } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import { useState } from 'react'
import useSWR from 'swr'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import GenericProductCard from '../GenericProductCard'
import theme from 'theme'

const ProductList = () => {
  const [page, setPage] = useState(1)
  const [productsPerPage, setProductsPerPage] = useState(10)

  const { data: products, error } = useSWR(`/products?page=${page}&productsPerPage=${productsPerPage}`)

  return (
    <PageLayout error={error} data={products}>
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}>
        <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <Typography variant="h5" fontWeight={900}>
            More For You
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
          <Typography variant="subtitle2">View all</Typography>
          <ArrowRightIcon />
        </Box>
      </Box>
      <Grid container gap={5}>
        {products?.result.map((product) => (
          <GenericProductCard key={product._id} product={product} />
        ))}
      </Grid>
      <Box display="flex" alignItems="center" justifyContent="space-between" mt={4}>
        <Pagination
          count={products?.pages}
          page={page}
          onChange={(event, value) => {
            setPage(value)
          }}
        />
        <Box display="flex" alignItems="center">
          <Typography variant="overline" sx={{ mr: 1 }}>
            Products per page:
          </Typography>
          <TextField
            size="small"
            select
            value={productsPerPage}
            onChange={(e) => {
              setPage(1)
              setProductsPerPage(e.target.value)
            }}
          >
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={20}>20</MenuItem>
            <MenuItem value={30}>30</MenuItem>
          </TextField>
        </Box>
      </Box>
    </PageLayout>
  )
}

export default ProductList
