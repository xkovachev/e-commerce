import { PageLayout } from 'layouts/Main/components'
import { Container, Box, Typography, Paper, CardMedia } from '@mui/material'
import theme from 'theme'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import WidgetsIcon from '@mui/icons-material/Widgets'
import { DUMMY_CATEGORIES } from 'utils/dummy-data/dummyCategories'

const CategoriesSection = () => {
  return (
    <PageLayout data={DUMMY_CATEGORIES}>
      <Container disableGutters>
        <Box
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <WidgetsIcon sx={{ color: theme.palette.secondary.alternate, fontSize: 30, mr: 1 }} />
            <Typography variant="h5" fontWeight={900}>
              Categories
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
            <Typography variant="subtitle2">View all</Typography>
            <ArrowRightIcon />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 2 }}>
          {DUMMY_CATEGORIES.map((category) => (
            <Paper
            key={category._id + 123}
              sx={{
                width: 186,
                gap: 1,
                p: 2,
                display: 'flex',
                alignItems: 'center',
                ':hover': {
                  boxShadow: 10,
                  cursor: 'pointer',
                },
              }}
            >
              <CardMedia component="img" src={category.image} sx={{ width: 52, height: 52 }} />
              <Typography fontWeight={600}>{category.name}</Typography>
            </Paper>
          ))}
        </Box>
      </Container>
    </PageLayout>
  )
}

export default CategoriesSection
