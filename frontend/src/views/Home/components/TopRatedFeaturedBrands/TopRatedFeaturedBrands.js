import { Container, Box, Typography, Paper, Card, CardMedia, Rating } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import theme from 'theme'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import StarBorderPurple500Icon from '@mui/icons-material/StarBorderPurple500'
import WorkspacePremiumIcon from '@mui/icons-material/WorkspacePremium'
import useSWR from 'swr'
import { DisplayCurrency } from 'components'
import { DUMMY_PRODUCTS } from 'utils/dummy-data/dummyProducts'
import { Link } from 'react-router-dom'

const TopRatedFeaturedBrands = (props) => {
  const { data, error } = useSWR('/products?page=1&productsPerPage=30')

  const topRatedProducts = data?.result?.sort((a, b) => b.rating - a.rating).slice(0, 4)

  return (
    <PageLayout data={data} error={error}>
      <Container disableGutters sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <Box sx={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              flexDirection: 'row',
              mb: 2,
              width: '100%',
              mr: 1,
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', mb: 1 }}>
              <WorkspacePremiumIcon sx={{ color: 'orange', fontSize: 30, mr: 1 }} />
              <Typography variant="h5" fontWeight={900}>
                Top Ratings
              </Typography>
            </Box>
            <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
              <Typography variant="subtitle2">View all</Typography>
              <ArrowRightIcon />
            </Box>
          </Box>
          <Paper sx={{ display: 'flex', flexDirection: 'row', gap: 2, justifyContent: 'space-between', p: 2, mr: 1 }}>
            {topRatedProducts?.map((product) => {
              return (
                <Card key={product._id} maxWidth={120}>
                  <Link to={`/products/${product._id}`}>
                    <CardMedia
                      component="img"
                      sx={{ height: 117, width: 117, ':hover': { opacity: 0.8, cursor: 'pointer' } }}
                      image={product.thumbnail}
                      alt={product.title}
                    />
                  </Link>
                  <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Rating size="small" value={product.rating} readOnly />
                    <Typography fontSize={13}>({Math.floor(Math.random() * 50 + 1)})</Typography>
                  </Box>
                  <Box
                    sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Typography textAlign="center" fontWeight={600}>
                      {product.brand.length > 15 ? 'Long title' : product.brand}
                    </Typography>
                    <Typography fontWeight={600} fontSize={14} sx={{ color: theme.palette.secondary.alternate }}>
                      <DisplayCurrency number={product.price} />
                    </Typography>
                  </Box>
                </Card>
              )
            })}
          </Paper>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              flexDirection: 'row',
              mb: 2,
              ml: 1,
              width: '100%',
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', mb: 1 }}>
              <StarBorderPurple500Icon sx={{ color: 'orange', fontSize: 30, mr: 1 }} />
              <Typography variant="h5" fontWeight={900}>
                Featured Brands
              </Typography>
            </Box>
            <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
              <Typography variant="subtitle2">View all</Typography>
              <ArrowRightIcon />
            </Box>
          </Box>
          <Paper
            sx={{
              display: 'flex',
              flexDirection: 'row',
              gap: 2,
              justifyContent: 'space-between',
              p: 2,
              ml: 1,
              height: 216.5,
            }}
          >
            {DUMMY_PRODUCTS.map((product, index) => {
              if (index > 1) return
              return (
                <Box key={product._id}>
                  <Box
                    sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Card>
                      <CardMedia
                        component="img"
                        sx={{ height: 150, width: 262, ':hover': { opacity: 0.8, cursor: 'pointer' } }}
                        image={product.images[0]}
                        alt={product.type}
                      />
                    </Card>
                  </Box>
                  <Typography fontWeight={600} sx={{ mt: 1, ml: 1 }}>
                    {product.type}
                  </Typography>
                </Box>
              )
            })}
          </Paper>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default TopRatedFeaturedBrands
