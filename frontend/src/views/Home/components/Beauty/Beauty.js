import { Container, Box, Typography, Paper, CardMedia, Grid, Tabs, Tab } from '@mui/material'
import theme from 'theme'
import useSWR from 'swr'
import { useState } from 'react'
import { PageLayout } from 'layouts/Main/components'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import GenericProductCard from '../GenericProductCard'
import { DUMMY_SHOPS } from 'utils/dummy-data/dummyShops'

const Beauty = (props) => {
  const { data, error } = useSWR('/products?page=1&productsPerPage=30')
  const products = data?.result?.filter((item) => item.category === 'fragrances' || item.category === 'skincare')

  const [currentTab, setCurrentTab] = useState(0)
  const handleTabChange = (event, newTab) => {
    setCurrentTab(newTab)
  }

  const TabPanel = ({ children, value, index }) => {
    return <div>{value === index && <>{children}</>}</div>
  }

  return (
    <PageLayout error={error} data={data}>
      <Container disableGutters sx={{ display: 'flex', flexDirection: 'row' }}>
        <Box sx={{ width: '25%', height: 520 }}>
          <Paper sx={{ width: '100%', height: '100%', gap: 2, p: 2, borderRadius: 2, position: 'relative' }}>
            <Box sx={{ width: '100%' }}>
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={currentTab} aria-label="basic tabs example" onChange={handleTabChange}>
                  <Tab label="Brands" />
                  <Tab label="Shops" />
                </Tabs>
                <TabPanel value={currentTab} index={0}>
                  {products?.map((product, index) => {
                    if (index > 5) return
                    return (
                      <Paper
                        key={product._id}
                        sx={{
                          gap: 1,
                          p: 2,
                          display: 'flex',
                          alignItems: 'center',
                          maxHeight: 44,
                          mt: 2,
                          mb: 1,
                          bgcolor: theme.palette.grey[200],
                          ':hover': {
                            boxShadow: 2,
                            cursor: 'pointer',
                          },
                        }}
                      >
                        <CardMedia component="img" src={product.thumbnail} sx={{ width: 20, height: 20 }} />
                        <Typography fontWeight={600}>{product.brand}</Typography>
                      </Paper>
                    )
                  })}
                </TabPanel>
                <TabPanel value={currentTab} index={1}>
                  {DUMMY_SHOPS.map((shop, index) => {
                    return (
                      <Paper
                        key={shop.name}
                        sx={{
                          gap: 1,
                          p: 2,
                          display: 'flex',
                          alignItems: 'center',
                          maxHeight: 44,
                          mt: 2,
                          mb: 1,
                          bgcolor: theme.palette.grey[200],
                          ':hover': {
                            boxShadow: 2,
                            cursor: 'pointer',
                          },
                        }}
                      >
                        <CardMedia component="img" src={shop.image} sx={{ width: 20, height: 20 }} />
                        <Typography fontWeight={600}>{shop.name}</Typography>
                      </Paper>
                    )
                  })}
                </TabPanel>
              </Box>
            </Box>

            <Paper
              sx={{
                bgcolor: '#F6F9FC',
                height: 44,
                position: 'absolute',
                bottom: 5,
                width: 208,
                left: 'auto',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                ':hover': {
                  boxShadow: 2,
                  cursor: 'pointer',
                },
              }}
            >
              <Typography textAlign="center" fontWeight={600}>
                View All {currentTab === 0 ? 'Brands' : 'Shops'}
              </Typography>
            </Paper>
          </Paper>
        </Box>
        <Box sx={{ width: '100%' }}>
          <Box
            sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', ml: 4 }}>
              <Typography variant="h5" fontWeight={900}>
                Beauty
              </Typography>
            </Box>
            <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
              <Typography variant="subtitle2">View all</Typography>
              <ArrowRightIcon />
            </Box>
          </Box>
          <Grid container gap={5} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            {products?.map((product, index) => {
              if (index > 8) return
              return <GenericProductCard key={product._id} product={product} />
            })}
          </Grid>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default Beauty
