import { Container, Box, Typography } from '@mui/material'
import theme from 'theme'
import { PageLayout } from 'layouts/Main/components'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import WidgetsIcon from '@mui/icons-material/Widgets'
import ProductCards from './ProductCards'

const TopCategoriesSwiper = (props) => {
  return (
    <PageLayout data={true}>
      <Container disableGutters>
        <Box
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <WidgetsIcon sx={{ color: theme.palette.secondary.alternate, fontSize: 30, mr: 1}} />
            <Typography variant="h5" fontWeight={900}>
              Top Categories
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
            <Typography variant="subtitle2">View all</Typography>
            <ArrowRightIcon />
          </Box>
        </Box>
        <ProductCards />
      </Container>
    </PageLayout>
  )
}

export default TopCategoriesSwiper
