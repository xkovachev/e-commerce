import { Container, CardMedia, Paper, Chip, Card, alpha } from '@mui/material'
import 'swiper/css'
import 'swiper/css/navigation'
import { styled } from '@mui/system'
import { Navigation, Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { useRef } from 'react'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import theme from 'theme'
import { DUMMY_PRODUCTS } from 'utils/dummy-data/dummyProducts'

const ProductCards = (props) => {
  const categorySwiperNavPrevEl = useRef()
  const categorySwiperNavNextEl = useRef()

  const LeftArrow = styled(ArrowBackIcon)({
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    maxWidth: 40,
    width: 'auto',
    height: 'auto',
    p: 10,
    fontSize: 12,
    position: 'absolute',
    top: 60,
    left: -25,
    zIndex: 3,
    ':hover': {
      cursor: 'pointer',
    },
  })

  const RightArrow = styled(ArrowForwardIcon)({
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    maxWidth: 40,
    width: 'auto',
    height: 'auto',
    p: 10,
    fontSize: 12,
    position: 'absolute',
    top: 60,
    right: -25,
    zIndex: 3,
    ':hover': {
      cursor: 'pointer',
    },
  })

  return (
    <Container disableGutters sx={{position: 'relative'}}>
      <Swiper
        modules={[Navigation, Pagination]}
        speed={800}
        spaceBetween={2}
        slidesPerView={3}
        loop
        onInit={(swiper) => {
          swiper.params.navigation.prevEl = categorySwiperNavPrevEl.current
          swiper.params.navigation.nextEl = categorySwiperNavNextEl.current
          swiper.navigation.init()
          swiper.navigation.update()
        }}
      >
        {DUMMY_PRODUCTS.map((product) => {
          return (
            <SwiperSlide key={product._id}>
              <Paper item xs={12} sm={6} md={3} sx={{ maxWidth: 380, maxHeight: 152, p: 2 }}>
                <Card
                  sx={{
                    width: '100%',
                    display: 'flex',
                    borderRadius: 1,
                    flexDirection: 'row',
                    bgcolor: theme.palette.grey[200],
                    ':hover': {
                      cursor: 'pointer',
                      opacity: 0.8,
                    },
                  }}
                >
                  <Chip
                    sx={{
                      position: 'absolute',
                      top: 30,
                      left: 15,
                      width: '25%',
                      maxHeight: 24,
                      bgcolor: theme.palette.primary.main,
                      color: 'white',
                      fontSize: 11,
                    }}
                    label={product.type}
                  />
                  <Chip
                    sx={{
                      position: 'absolute',
                      top: 30,
                      right: 15,
                      width: '35%',
                      maxHeight: 24,
                      bgcolor: alpha('#000000', 0.05),
                      fontSize: 11,
                    }}
                    label={'3k orders this week'}
                  />
                  {product.images.map((imageSrc, index) => {
                    if (index > 4) return
                    return (
                      <CardMedia
                        key={imageSrc + index}
                        component="img"
                        sx={{ width: '33%', height: 120, backgroundColor: theme.palette.grey[200] }}
                        image={imageSrc}
                        alt={product.type}
                      />
                    )
                  })}
                </Card>
              </Paper>
            </SwiperSlide>
          )
        })}
      </Swiper>
      <LeftArrow ref={categorySwiperNavPrevEl} />
      <RightArrow ref={categorySwiperNavNextEl} />
    </Container>
  )
}

export default ProductCards
