import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import 'swiper/css/scrollbar'
import { Container, styled } from '@mui/system'
import { Navigation, Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Grid, Typography, Box, Button } from '@mui/material'
import theme from '../../../../theme'

const Slide = styled(SwiperSlide)({
  width: '100%',
  height: '100%',
  objectFit: 'cover',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

const Image = styled('img')({
  maxWidth: 530,
  width: '100%',
  maxHeight: 400,
  height: '100%',
})

const customSlide = (
  <Slide>
    <Grid sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
      <Box
        sx={{
          maxWidth: 463,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'flex-start',
          flexDirection: 'column',
          maxHeight: 424,
          gap: 2,
          pl: 8,
          pt: 1,
        }}
      >
        <Typography fontSize={50} fontWeight={900} sx={{ maxHeight: 154, height: '100%' }}>
          50% Off For Your First Shopping
        </Typography>
        <Typography fontSize={14}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis lobortis consequat eu, quam etiam at quis ut
          convalliss.
        </Typography>
        <Button
          sx={{
            bgcolor: theme.palette.secondary.alternate,
            maxWidth: 154,
            maxHeight: 50,
            fontSize: 14,
            color: 'white',
            width: '100%',
            height: '100%',
            ':hover': {
              bgcolor: '#dc324e',
            },
          }}
        >
          Shop Now
        </Button>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', pr: 12.45, pt: 2 }}>
        <Image src="https://bazaar.ui-lib.com/assets/images/products/nike-black.png" />
      </Box>
    </Grid>
  </Slide>
)

const ShoeSwiper = (props) => {
  return (
    <Container
      disableGutters
      maxWidth={1280}
      maxHeight={520}
      sx={{ bgcolor: 'background.default', display: 'flex', alignItems: 'center', width: '100%', height: '100%' }}
    >
      <Container sx={{ width: '100%', mt: 2, height: '100%' }}>
        <Swiper
          modules={[Navigation, Pagination]}
          speed={800}
          slidesPerView={1}
          pagination={{
            clickable: true,
          }}
        >
          {customSlide}
          {customSlide}
        </Swiper>
      </Container>
    </Container>
  )
}

export default ShoeSwiper
