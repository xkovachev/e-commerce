import {
  Container,
  Box,
  ButtonBase,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Paper,
  IconButton,
  Rating,
  Tooltip,
  Typography,
  Chip,
} from '@mui/material'
import 'swiper/css'
import 'swiper/css/navigation'
import { styled } from '@mui/system'
import { Navigation, Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { DisplayCurrency } from 'components'
import { useCart } from 'core'
import { useRef } from 'react'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import theme from 'theme'
import { Link } from 'react-router-dom'

const ProductCards = ({ products }) => {
  const { addToCart, removeFromCart, cart } = useCart()

  const discountedProducts = products
    .filter((product) => product.discountPercentage > 0)
    .sort((a, b) => a.rating - b.rating)

  const swiperNavPrevEl = useRef()
  const swiperNavNextEl = useRef()

  const LeftArrow = styled(ArrowBackIcon)({
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    maxWidth: 40,
    width: 'auto',
    height: 'auto',
    p: 10,
    fontSize: 12,
    position: 'absolute',
    top: 150,
    left: -25,
    zIndex: 1,
    ':hover': {
      cursor: 'pointer',
    },
  })

  const RightArrow = styled(ArrowForwardIcon)({
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    maxWidth: 40,
    width: 'auto',
    height: 'auto',
    p: 10,
    fontSize: 12,
    position: 'absolute',
    top: 150,
    right: -25,
    zIndex: 1,
    ':hover': {
      cursor: 'pointer',
    },
  })

  return (
    <Container disableGutters sx={{ position: 'relative' }}>
      <Swiper
        modules={[Navigation, Pagination]}
        speed={800}
        spaceBetween={40}
        slidesPerView={4}
        onInit={(swiper) => {
          swiper.params.navigation.prevEl = swiperNavPrevEl.current
          swiper.params.navigation.nextEl = swiperNavNextEl.current
          swiper.navigation.init()
          swiper.navigation.update()
        }}
        loop
      >
        {discountedProducts?.map((product) => {
          return (
            <SwiperSlide key={product._id}>
              <Paper
                item
                xs={12}
                sm={4}
                md={3}
                sx={{ width: 270, height: 400, ':hover': { cursor: 'pointer' } }}
                key={product._id}
              >
                <Card sx={{ height: 400 }}>
                  <Chip
                    sx={{
                      position: 'absolute',
                      top: 10,
                      left: 10,
                      width: '25%',
                      maxHeight: 24,
                      bgcolor: theme.palette.secondary.alternate,
                      color: 'white',
                      fontSize: 11,
                    }}
                    label={`${product.discountPercentage.toFixed(0)}% off`}
                  />
                  <Box sx={{ position: 'absolute', top: 2, right: 2, display: 'flex', flexDirection: 'column' }}>
                    <IconButton>
                      <RemoveRedEyeIcon sx={{ color: theme.palette.grey[300] }} />
                    </IconButton>
                    <IconButton>
                      <FavoriteBorderIcon sx={{ color: theme.palette.grey[300] }} />
                    </IconButton>
                  </Box>
                  <Link to={`/products/${product._id}`}>
                    <CardMedia
                      component="img"
                      sx={{ width: 287, height: 287 }}
                      image={product.thumbnail}
                      alt={product.title}
                    />
                  </Link>
                  <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                    <CardContent>
                      <Tooltip title={product.title} placement="bottom-start">
                        <Typography gutterBottom variant="h6" component="div" noWrap fontSize={14} fontWeight={600}>
                          {product.title.length <= 26 ? product.title : `${product.title.slice(0, 26)}...`}
                        </Typography>
                      </Tooltip>
                      <Rating name="read-only" value={product.rating} readOnly size="small" sx={{ mr: 1 }} />
                      <Typography variant="body2" component="div">
                        <Box display="flex" alignItems="center">
                          <Box sx={{ color: theme.palette.secondary.alternate, fontWeight: 600 }}>
                            <DisplayCurrency
                              number={product.price - (product.price / 100) * product.discountPercentage}
                            />
                          </Box>
                          <Box sx={{ textDecoration: 'line-through', ml: 1, color: 'gray', fontWeight: 600 }}>
                            <DisplayCurrency number={product.price} />
                          </Box>
                        </Box>
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Box display="flex" justifyContent="space-between" alignItems="flex-end" sx={{ width: '100%' }}>
                        <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                          {cart.findIndex((item) => item._id === product._id) !== -1 && (
                            <>
                              <ButtonBase
                                sx={{
                                  color: theme.palette.secondary.alternate,
                                  border: `1px solid ${theme.palette.secondary.alternate}`,
                                  borderRadius: 1,
                                  width: 28,
                                  height: 28,
                                  fontSize: 20,
                                }}
                                onClick={() => removeFromCart(product._id)}
                              >
                                -
                              </ButtonBase>
                              <Box>
                                <Typography textAlign="center">
                                  {cart.map((item) => {
                                    if (item._id === product._id) {
                                      return item.quantity
                                    }
                                  })}
                                </Typography>
                              </Box>
                            </>
                          )}
                          <ButtonBase
                            sx={{
                              color: theme.palette.secondary.alternate,
                              border: `1px solid ${theme.palette.secondary.alternate}`,
                              borderRadius: 1,
                              width: 28,
                              height: 28,
                              fontSize: 20,
                            }}
                            onClick={() => addToCart(product)}
                          >
                            +
                          </ButtonBase>
                        </Box>
                      </Box>
                    </CardActions>
                  </Box>
                </Card>
              </Paper>
            </SwiperSlide>
          )
        })}
      </Swiper>
      <LeftArrow ref={swiperNavPrevEl} />
      <RightArrow ref={swiperNavNextEl} />
    </Container>
  )
}

export default ProductCards
