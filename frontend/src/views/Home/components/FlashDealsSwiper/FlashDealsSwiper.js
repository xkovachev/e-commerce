import ProductCards from './ProductCards/ProductCards'
import BoltIcon from '@mui/icons-material/Bolt'
import { Container, Box, Typography } from '@mui/material'
import theme from 'theme'
import useSWR from 'swr'
import { PageLayout } from 'layouts/Main/components'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'

const FlashDealsSwiper = (props) => {
  const { data: products, error } = useSWR('/products?page=1&productsPerPage=30')

  return (
    <PageLayout error={error} data={products}>
      <Container disableGutters>
        <Box
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <BoltIcon sx={{ color: theme.palette.secondary.alternate, fontSize: 30 }} />
            <Typography variant="h5" fontWeight={900}>
              Flash Deals
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
            <Typography variant="subtitle2">View all</Typography>
            <ArrowRightIcon />
          </Box>
        </Box>
        {products?.result && <ProductCards products={products.result} />}
      </Container>
    </PageLayout>
  )
}

export default FlashDealsSwiper
