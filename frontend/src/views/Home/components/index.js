export { default as ProductList } from './ProductList'
export { default as MenuBar } from '../../../layouts/Main/components/MenusBar'
export { default as ShoeSwiper } from './ShoeSwiper'
export { default as FlashDealsSwiper } from './FlashDealsSwiper'
export { default as TopCategoriesSwiper } from './TopCategoriesSwiper'
export { default as TopRatedFeaturedBrands } from './TopRatedFeaturedBrands'
export { default as NewArrivals } from './NewArrivals'
export { default as BigDiscounts } from './BigDiscounts'
export { default as GenericProductCard } from './GenericProductCard'
export { default as CategoriesSection } from './CategoriesSection'
export { default as Electronics } from './Electronics'
export { default as Beauty } from './Beauty'
export { default as MiscProductsSection } from './MiscProductsSection'
export { default as ShopTraits } from './ShopTraits'
export { default as CustomSettingsIcon } from './SettingsIcon'
