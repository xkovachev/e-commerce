import ProductCards from './ProductCards/ProductCards'
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard'
import { Container, Box, Typography } from '@mui/material'
import theme from 'theme'
import useSWR from 'swr'
import { PageLayout } from 'layouts/Main/components'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'

const FlashDealsSwiper = (props) => {
  const { data: products, error } = useSWR('/products?page=1&productsPerPage=30')

  const discountedProducts = products?.result
    ?.sort((a, b) => b.discountPercentage - a.discountPercentage)
    .filter((product) => product.discountPercentage > 0)

  return (
    <PageLayout error={error} data={products}>
      <Container disableGutters>
        <Box
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <CardGiftcardIcon sx={{ color: theme.palette.secondary.alternate, fontSize: 30, mr: 1 }} />
            <Typography variant="h5" fontWeight={900}>
              Big Discounts
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
            <Typography variant="subtitle2">View all</Typography>
            <ArrowRightIcon />
          </Box>
        </Box>
        {products?.result && <ProductCards products={discountedProducts} />}
      </Container>
    </PageLayout>
  )
}

export default FlashDealsSwiper
