import { Container, Box, CardActions, CardContent, CardMedia, Paper, Tooltip, Typography } from '@mui/material'
import 'swiper/css'
import 'swiper/css/navigation'
import { styled } from '@mui/system'
import { Navigation, Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { DisplayCurrency } from 'components'
import { useRef } from 'react'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import theme from 'theme'
import { Link } from 'react-router-dom'

const ProductCards = ({ products }) => {
  const discountSwiperNavPrevEl = useRef()
  const discountSwiperNavNextEl = useRef()

  const LeftArrow = styled(ArrowBackIcon)({
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    maxWidth: 40,
    width: 'auto',
    height: 'auto',
    p: 10,
    fontSize: 12,
    position: 'absolute',
    top: 100,
    left: -25,
    zIndex: 1,
    ':hover': {
      cursor: 'pointer',
    },
  })

  const RightArrow = styled(ArrowForwardIcon)({
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    maxWidth: 40,
    width: 'auto',
    height: 'auto',
    p: 10,
    fontSize: 12,
    position: 'absolute',
    top: 100,
    right: -25,
    zIndex: 1,
    ':hover': {
      cursor: 'pointer',
    },
  })

  return (
    <Container disableGutters sx={{position: 'relative'}}>
      <Swiper
        modules={[Navigation, Pagination]}
        speed={800}
        spaceBetween={80}
        slidesPerView={6}
        onInit={(swiper) => {
          swiper.params.navigation.prevEl = discountSwiperNavPrevEl.current
          swiper.params.navigation.nextEl = discountSwiperNavNextEl.current
          swiper.navigation.init()
          swiper.navigation.update()
        }}
        loop
      >
        {products?.map((product) => {
          return (
            <SwiperSlide key={product._id}>
              <Paper
                item
                xs={12}
                sm={4}
                md={3}
                maxWidth={184}
                sx={{
                  width: 184,
                  height: 238,
                  p: 1,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'column',
                  ':hover': {
                    cursor: 'pointer',
                  },
                }}
                key={product._id}
              >
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Link to={`/products/${product._id}`}>

                  <CardMedia
                    component="img"
                    sx={{ width: 152, height: 152, borderRadius: 1, ':hover': { opacity: 0.8 } }}
                    image={product.thumbnail}
                    alt={product.title}
                  />
                  </Link>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'start', alignItems: 'start', mt: 1 }}>
                  <Tooltip title={product.title} placement="bottom-start">
                    <Typography component="div" fontWeight={600} noWrap>
                      {product.brand}
                    </Typography>
                  </Tooltip>
                </Box>
                <CardActions>
                  <Box display="flex" justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
                    <Typography variant="body2" component="div">
                      {product.discountPercentage > 0 && (
                        <Box display="flex" alignItems="center">
                          <Box sx={{ color: theme.palette.secondary.alternate, fontWeight: 'bold' }}>
                            <DisplayCurrency
                              number={product.price - (product.price / 100) * product.discountPercentage}
                            />
                          </Box>
                          <Box sx={{ ml: 1, textDecoration: 'line-through', color: 'gray', fontWeight: 'bold' }}>
                            <DisplayCurrency number={product.price} />
                          </Box>
                        </Box>
                      )}
                    </Typography>
                  </Box>
                </CardActions>
              </Paper>
            </SwiperSlide>
          )
        })}
      </Swiper>
      <LeftArrow ref={discountSwiperNavPrevEl} />
      <RightArrow ref={discountSwiperNavNextEl} />
    </Container>
  )
}

export default ProductCards
