import { Container, Box, Typography, Paper, Card, CardMedia } from '@mui/material'
import theme from 'theme'
import { PageLayout } from 'layouts/Main/components'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import FiberNewIcon from '@mui/icons-material/FiberNew'
import useSWR from 'swr'
import { DisplayCurrency } from 'components'
import { Link } from 'react-router-dom'

const NewArrivals = (props) => {
  const { data, error } = useSWR('/products?page=1&productsPerPage=30')
  const latestProducts = data?.result?.slice(-6)

  return (
    <PageLayout data={data} error={error}>
      <Container disableGutters>
        <Box
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <FiberNewIcon sx={{ color: 'limegreen', fontSize: 30, mr: 1 }} />
            <Typography variant="h5" fontWeight={900}>
              New Arrivals
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
            <Typography variant="subtitle2">View all</Typography>
            <ArrowRightIcon />
          </Box>
        </Box>
        <Box>
          <Paper
            sx={{
              width: '100%',
              height: 265,
              display: 'flex',
              p: 2,
              gap: 2.5,
            }}
          >
            {latestProducts?.map((product) => {
              return (
                <Box key={product._id} sx={{ display: 'flex', flexDirection: 'column' }}>
                  <Link to={`/products/${product._id}`}>
                    <CardMedia
                      component="img"
                      sx={{
                        width: 178,
                        height: 178,
                        borderRadius: 2,
                        backgroundColor: theme.palette.grey[200],

                        ':hover': {
                          cursor: 'pointer',
                          opacity: 0.8,
                        },
                      }}
                      image={product.thumbnail}
                      alt={product.title}
                    />
                  </Link>
                  <Box sx={{ display: 'flex', flexDirection: 'column', mt: 1 }}>
                    <Typography fontWeight={600} fontSize={14}>
                      {product.brand.length > 15 ? 'Long title' : product.brand}
                    </Typography>
                    <Typography fontWeight={600} fontSize={14} sx={{ color: theme.palette.secondary.alternate }}>
                      <DisplayCurrency number={product.price} />
                    </Typography>
                  </Box>
                </Box>
              )
            })}
          </Paper>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default NewArrivals
