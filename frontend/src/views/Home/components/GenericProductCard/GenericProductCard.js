import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  IconButton,
  Rating,
  Tooltip,
  Typography,
  ButtonBase,
  Paper,
  Chip,
} from '@mui/material'
import { DisplayCurrency } from 'components'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import theme from 'theme'
import { Link } from 'react-router-dom'
import { useCart } from 'core'

const GenericProductCard = ({ product }) => {
  const { cart, removeFromCart, addToCart } = useCart()

  return (
    <Paper
      item
      xs={12}
      sm={4}
      md={3}
      sx={{ width: 270, height: 400, ':hover': { boxShadow: 10, cursor: 'pointer' } }}
      key={product._id}
    >
      <Card sx={{ height: 400, position: 'relative' }}>
        {product.discountPercentage > 0 && (
          <Chip
            sx={{
              position: 'absolute',
              top: 10,
              left: 10,
              width: '25%',
              maxHeight: 24,
              bgcolor: theme.palette.secondary.alternate,
              color: 'white',
              fontSize: 11,
            }}
            label={`${product.discountPercentage.toFixed(0)}% off`}
          />
        )}
        <Box sx={{ position: 'absolute', top: 2, right: 2, display: 'flex', flexDirection: 'column' }}>
          <IconButton>
            <RemoveRedEyeIcon sx={{ color: theme.palette.grey[300] }} />
          </IconButton>
          <IconButton>
            <FavoriteBorderIcon sx={{ color: theme.palette.grey[300] }} />
          </IconButton>
        </Box>
        <Link to={`/products/${product._id}`}>
          <CardMedia component="img" sx={{ width: 287, height: 287 }} image={product.thumbnail} alt={product.title} />
        </Link>
        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end' }}>
          <CardContent>
            <Tooltip title={product.title} placement="bottom-start">
              <Typography gutterBottom variant="h6" component="div" noWrap fontSize={14} fontWeight={600}>
                {product.title.length <= 26 ? product.title : `${product.title.slice(0, 26)}...`}
              </Typography>
            </Tooltip>
            <Rating name="read-only" value={product.rating} readOnly size="small" sx={{ mr: 1 }} />
            <Typography variant="body2" component="div">
              {product.discountPercentage > 0 ? (
                <>
                  <Box display="flex" alignItems="center">
                    <Box sx={{ color: theme.palette.secondary.alternate, fontWeight: 600 }}>
                      <DisplayCurrency number={product.price - (product.price / 100) * product.discountPercentage} />
                    </Box>
                    <Box sx={{ textDecoration: 'line-through', ml: 1, color: 'gray', fontWeight: 600 }}>
                      <DisplayCurrency number={product.price} />
                    </Box>
                  </Box>
                </>
              ) : (
                <Box sx={{ color: theme.palette.secondary.alternate, fontWeight: 600 }}>
                  <DisplayCurrency number={product.price} />
                </Box>
              )}
            </Typography>
          </CardContent>
          <CardActions>
            <Box display="flex" justifyContent="space-between" alignItems="flex-end" sx={{ width: '100%' }}>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                {cart.find((item) => item._id === product._id) && (
                  <>
                    <ButtonBase
                      sx={{
                        color: theme.palette.secondary.alternate,
                        border: `1px solid ${theme.palette.secondary.alternate}`,
                        borderRadius: 1,
                        width: 28,
                        height: 28,
                        fontSize: 20,
                      }}
                      onClick={() => removeFromCart(product._id)}
                    >
                      -
                    </ButtonBase>
                    <Box>
                      <Typography textAlign="center">
                        {cart.map((item) => {
                          if (item._id === product._id) {
                            return item.quantity
                          }
                        })}
                      </Typography>
                    </Box>
                  </>
                )}
                <ButtonBase
                  sx={{
                    color: theme.palette.secondary.alternate,
                    border: `1px solid ${theme.palette.secondary.alternate}`,
                    borderRadius: 1,
                    width: 28,
                    height: 28,
                    fontSize: 20,
                  }}
                  onClick={() => addToCart(product)}
                >
                  +
                </ButtonBase>
              </Box>
            </Box>
          </CardActions>
        </Box>
      </Card>
    </Paper>
  )
}

export default GenericProductCard
