import { Container, Box, Typography, Paper, CardMedia, Grid } from '@mui/material'
import theme from 'theme'
import useSWR from 'swr'
import { PageLayout } from 'layouts/Main/components'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import GenericProductCard from '../GenericProductCard'

const MiscProductsSection = () => {
  const { data, error } = useSWR('/products?page=1&productsPerPage=30')
  const products = data?.result?.filter((item) => item.category === 'groceries' || item.category === 'home-decoration')

  return (
    <PageLayout error={error} data={data}>
      <Container disableGutters sx={{ display: 'flex', flexDirection: 'row' }}>
        <Box sx={{ width: '25%', height: 472 }}>
          <Paper sx={{ width: '100%', height: '100%', gap: 2, p: 2, borderRadius: 2 }}>
            {products?.map((product, index) => {
              if (index > 5) return
              return (
                <Paper
                  key={product._id}
                  sx={{
                    gap: 1,
                    p: 2,
                    display: 'flex',
                    alignItems: 'center',
                    maxHeight: 44,
                    mb: 1,
                    bgcolor: theme.palette.grey[200],
                    ':hover': {
                      boxShadow: 2,
                      cursor: 'pointer',
                    },
                  }}
                >
                  <CardMedia component="img" src={product.thumbnail} sx={{ width: 20, height: 20 }} />
                  <Typography fontWeight={600}>{product.brand}</Typography>
                </Paper>
              )
            })}
            <Paper
              sx={{
                bgcolor: '#F6F9FC',
                height: 44,
                mt: 10,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                ':hover': {
                  boxShadow: 2,
                  cursor: 'pointer',
                },
              }}
            >
              <Typography textAlign="center">View All Brands</Typography>
            </Paper>
          </Paper>
        </Box>
        <Box sx={{ width: '100%' }}>
          <Box
            sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', mb: 2 }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', ml: 4 }}>
              <Typography variant="h5" fontWeight={900}>
                Groceries & Home Decoration
              </Typography>
            </Box>
            <Box sx={{ display: 'flex', color: theme.palette.grey[500], ':hover': { cursor: 'pointer' } }}>
              <Typography variant="subtitle2">View all</Typography>
              <ArrowRightIcon />
            </Box>
          </Box>
          <Grid container gap={5} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            {products?.map((product, index) => {
              if (index > 5) return
              return <GenericProductCard key={product._id} product={product} />
            })}
          </Grid>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default MiscProductsSection
