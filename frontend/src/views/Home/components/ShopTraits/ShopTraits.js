import { Grid, Paper, Box, ButtonBase, Container, Typography } from '@mui/material'
import theme from 'theme'
import LocalShippingIcon from '@mui/icons-material/LocalShipping'
import CreditScoreIcon from '@mui/icons-material/CreditScore'
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser'
import HeadsetMicIcon from '@mui/icons-material/HeadsetMic'

const ShopTraits = () => {
  const traits = [
    { title: 'Worldwide Delivery', icon: <LocalShippingIcon sx={{ fontSize: 25, color: 'gray' }} /> },
    { title: 'Safe Payment', icon: <CreditScoreIcon sx={{ fontSize: 25, color: 'gray' }} /> },
    { title: 'Shop With Confidence', icon: <VerifiedUserIcon sx={{ fontSize: 25, color: 'gray' }} /> },
    { title: '24/7 Support', icon: <HeadsetMicIcon sx={{ fontSize: 25, color: 'gray' }} /> },
  ]

  const dummyText = 'We offer competitive prices on our 100 million plus product any range.'

  const TraitCard = ({ trait }) => {
    return (
      <Paper
        sx={{
          maxWidth: 288,
          maxHeight: 279,
          p: 6,
          gap: 2,
          height: '100%',
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 2,
          ':hover': {
            boxShadow: 3,
          }
        }}
      >
        <ButtonBase sx={{ bgcolor: theme.palette.grey[200], borderRadius: '50%', width: 70, height: 70 }}>
          {trait.icon}
        </ButtonBase>
        <Typography fontWeight={600} textAlign="center">
          {trait.title}
        </Typography>
        <Typography fontSize={14} textAlign="center" sx={{ color: 'gray' }}>
          {dummyText}
        </Typography>
      </Paper>
    )
  }

  return (
    <Container disableGutters sx={{ mt: 5, mb: 10 }}>
      <Grid sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        {traits.map((trait) => (
          <TraitCard trait={trait} />
        ))}
      </Grid>
    </Container>
  )
}

export default ShopTraits
