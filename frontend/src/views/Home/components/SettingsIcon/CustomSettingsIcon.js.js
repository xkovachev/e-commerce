import SettingsIcon from '@mui/icons-material/Settings'
import { Box, Tooltip, IconButton } from '@mui/material'

const CustomSettingsIcon = () => {
  return (
    <Box
      sx={{
        position: 'fixed',
        top: 720,
        right: 50,
        bgcolor: 'secondary.alternate',
        width: 48,
        height: 48,
        borderRadius: '50%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        boxShadow: 10,
        ':hover': {
          cursor: 'pointer',
        },
      }}
    >
      <Tooltip title="Settings & Demos">
        <IconButton>
          <SettingsIcon sx={{ color: 'white' }} />
        </IconButton>
      </Tooltip>
    </Box>
  )
}

export default CustomSettingsIcon
