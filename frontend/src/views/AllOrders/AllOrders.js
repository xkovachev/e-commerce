import ShoppingBagIcon from '@mui/icons-material/ShoppingBag'
import {
  Box,
  Container,
  Typography,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  IconButton,
  Pagination,
} from '@mui/material'
import { useCart } from 'core'
import theme from 'theme'
import { useState } from 'react'
import { DisplayCurrency } from 'components'
import { Link } from 'react-router-dom'
import { PageLayout } from 'layouts/Main/components'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import useSWR from 'swr'

const AllOrders = () => {
  const [page, setPage] = useState(0)
  const { getFinalPrice } = useCart()
  const { data, error } = useSWR(`/checkouts?page=${page}`)

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const columns = [
    { id: 'orderId', label: 'Order #' },
    { id: 'customer', label: 'Customer' },
    { id: 'method', label: 'Payment Method' },
    { id: 'date', label: 'Date purchased' },
    { id: 'total', label: 'Total' },
    { id: 'viewOrder', label: '' },
  ]

  return (
    <PageLayout data={data} error={error}>
      <Container disableGutters>
        <Box sx={{ display: 'flex', alignItems: 'center', mt: 2 }}>
          <ShoppingBagIcon sx={{ color: theme.palette.secondary.alternate }} />
          <Typography sx={{ fontWeight: 'bold', fontSize: 20 }}>&nbsp;All Orders</Typography>
        </Box>
        <Box sx={{ my: 2 }}>
          <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell key={column.id} align="center" sx={{ fontWeight: 'bold' }}>
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data?.result?.map((order) => {
                    console.log(order)
                    return (
                      <TableRow key={order?._id}>
                        <TableCell align="center">{order?._id}</TableCell>
                        <TableCell align="center">{order?.address?.fullName}</TableCell>

                        <TableCell align="center">{order?.payment?.type}</TableCell>
                        <TableCell align="center">
                          {new Date(order?.createdAt).toLocaleDateString('en-GB', {
                            year: 'numeric',
                            month: 'short',
                            day: 'numeric',
                          })}
                        </TableCell>
                        <TableCell align="center">
                          <DisplayCurrency
                            number={order?.cart.reduce((accumulator, currentValue) => {
                              return accumulator + getFinalPrice(currentValue.product)
                            }, 0)}
                          />
                        </TableCell>
                        <TableCell align="center">
                          <Link to={`/order/${order?._id}`} style={{ textDecoration: 'none', color: 'black' }}>
                            <IconButton>
                              <ArrowForwardIcon />
                            </IconButton>
                          </Link>
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <Box sx={{ display: 'flex', justifyContent: 'center', alignContent: 'center', my: 2 }}>
              <Pagination count={Math.ceil(data?.total / 10) || 10} page={page} onChange={handleChangePage} />
            </Box>
          </Paper>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default AllOrders
