import * as yup from 'yup'

export const validationSchemas = {
  1: yup.object().shape({
    address: yup.object().shape({
      email: yup.string('Enter your email').email('Enter a valid email').required('Email is required'),
      country: yup.string('Enter your country').required('Country is required'),
      fullName: yup
        .string('Enter your full name')
        .matches(/[A-Za-z ]+/, 'You full name may contain only Latin letters')
        .required('Full name is required'),
      phone: yup
        .string('Enter you phone number')
        .matches(/^[0-9]+$/, 'You phone number may include digits only')
        .min(6, 'Phone number should consists of 6 digits minimum')
        .required('Phone number is required'),
      company: yup.string('Enter company'),
      zip: yup
        .string('Enter company')
        .matches(/^[0-9]+$/, 'You Zip code may include digits only')
        .min(4, 'Zip code should consist of 4 digits minmum')
        .required('Zip code is required'),
      address1: yup.string('Enter street name & number').required('Street name and number are required'),
      address2: yup.string('Enter entrance, floor or apartment details'),
    }),
  }),
  2: yup.object().shape({
    payment: yup.object().shape({
      type: yup.mixed().oneOf(['delivery', 'card', 'paypal']).required('Type of payment is required'),
      card: yup.object().when('type', {
        is: (card) => card !== 'card',
        then: yup.object().shape({
          name: yup.string(),
          expiryDate: yup.string(),
          number: yup.number(),
          ccv: yup.number(),
        }),
        otherwise: yup.object().shape({
          name: yup.string().required('Name on card is required'),
          expiryDate: yup.string().required('Expiry date is required'),
          number: yup
            .string()
            .min(16, 'Your card number must consist of 16 digits.')
            .max(16, 'Your card number must consist of 16 digits.')
            .required('Card number is required'),
          ccv: yup
            .string()
            .min(3, 'CCV must consist of 3 digits.')
            .max(3, 'CCV must consist of 3 digits.')
            .required('CCV is required'),
        }),
      }),
      details: yup.string(),
    }),
  }),
}
