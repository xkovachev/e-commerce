import { Box, Button, Paper, Step, StepLabel, Stepper } from '@mui/material'
import { useFormik } from 'formik'
import { PageLayout } from 'layouts/Main/components'
import { Fragment, useState } from 'react'
import Cart from 'views/Cart'
import { Payment, Shipping } from './components'
import { API_URL } from 'config'
import axios from 'axios'
import { useCart } from 'core'
import { validationSchemas } from './validationSchemas'
import { useError } from 'utils/hooks'
import { useSnackbar } from 'notistack'
import { PageURLs } from 'Routes'
import { useNavigate } from 'react-router-dom'

const Checkout = () => {
  const { setError } = useError()
  const { cart: originalCart, resetCart } = useCart()
  const { enqueueSnackbar } = useSnackbar()
  const navigate = useNavigate()

  const [activeStep, setActiveStep] = useState(0)

  const formik = useFormik({
    initialValues: {
      address: {
        fullName: '',
        phone: '',
        email: '',
        zip: '',
        address1: '',
        address2: '',
        country: '',
      },
      payment: {
        type: 'delivery',
        card: {
          number: '',
          name: '',
          expiryDate: '',
          ccv: '',
        },
        details: '',
      },
    },
    validationSchema: activeStep in validationSchemas ? validationSchemas[activeStep] : null,
    onSubmit: async (values) => {
      const cart = originalCart.map((item) => {
        return { product: item._id, quantity: item.quantity }
      })

      try {
        const { data } = await axios.post(`${API_URL}/checkouts/create`, { cart, ...values })
        enqueueSnackbar(data.message, {
          variant: 'success',
        })
        resetCart()
        navigate(`${PageURLs.Order}/${data.id}`)
      } catch (error) {
        setError(error)
      }

      try {
        const { data } = await axios.get(`/checkouts`)
        console.log(data)
      } catch (error) {
        console.log(error)
      }
    },
  })

  const steps = [
    { label: 'Cart', component: <Cart /> },
    { label: 'Shipping', component: <Shipping formik={formik} /> },
    { label: 'Payment', component: <Payment formik={formik} /> },
  ]

  const handleNext = () => {
    if (activeStep === steps.length - 1) {
      formik.handleSubmit()
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep + 1)
    }
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  return (
    <PageLayout isAsync={false} container>
      <Paper sx={{ width: '100%', p: 2 }}>
        <Stepper activeStep={activeStep} sx={{ mb: 4 }}>
          {steps.map((step, index) => {
            return (
              <Step key={step.label}>
                <StepLabel>{step.label}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
        <Fragment>
          <Box sx={{ mt: 2, mb: 1 }}>{steps[activeStep].component}</Box>
          <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
            <Button color="inherit" disabled={activeStep === 0} onClick={handleBack} sx={{ mr: 1 }} variant="contained">
              Back
            </Button>
            <Box sx={{ flex: '1 1 auto' }} />
            <Button
              variant="contained"
              onClick={handleNext}
              color="primary"
              disabled={(activeStep !== 0 && !(formik.isValid && formik.dirty)) || originalCart.length === 0}
            >
              {activeStep === steps.length - 1 ? 'Place order' : `Proceed to ${steps[activeStep + 1].label}`}
            </Button>
          </Box>
        </Fragment>
      </Paper>
    </PageLayout>
  )
}

export default Checkout
