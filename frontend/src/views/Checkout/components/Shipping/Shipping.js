import { Divider, Paper, TextField, Typography, Grid, Container } from '@mui/material'
import { PricingOverview } from 'components'
import { useCart } from 'core'

const Shipping = ({ formik }) => {
  const { cart } = useCart()
  return (
    <Container disableGutters sx={{ display: 'flex', mb: 2, gap: 2 }}>
      <Paper sx={{ p: 2, maxWidth: 783, width: '100%' }}>
        <Typography variant="h6" align="center">
          Shipping Address
        </Typography>
        <Divider sx={{ my: 2 }} />

        <Grid container sx={{ gap: 2 }}>
          <Grid item sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}>
            <TextField
              fullWidth
              name="address.fullName"
              label="Full Name"
              size="small"
              value={formik.values.address?.fullName}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.fullName && Boolean(formik.errors.address?.fullName)}
              helperText={formik.touched.address?.fullName && formik.errors.address?.fullName}
              sx={{ mb: 2, maxWidth: 340 }}
            />

            <TextField
              fullWidth
              label="Address 1"
              name="address.address1"
              size="small"
              value={formik.values.address?.address1}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.address1 && Boolean(formik.errors.address?.address1)}
              helperText={formik.touched.address?.address1 && formik.errors.address?.address1}
              sx={{ mb: 1, maxWidth: 340 }}
            />
          </Grid>

          <Grid item sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}>
            <TextField
              fullWidth
              id="email"
              label="Email"
              name="address.email"
              size="small"
              value={formik.values.address?.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.email && Boolean(formik.errors.address?.email)}
              helperText={formik.touched.address?.email && formik.errors.address?.email}
              sx={{ mb: 1, maxWidth: 340 }}
            />

            <TextField
              fullWidth
              id="address2"
              label="Address 2"
              name="address.address2"
              size="small"
              value={formik.values.address?.address2}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.address2 && Boolean(formik.errors.address?.address2)}
              helperText={formik.touched.address?.address2 && formik.errors.address?.address2}
              sx={{ mb: 1, maxWidth: 340 }}
            />
          </Grid>

          <Grid item sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}>
            <TextField
              fullWidth
              id="phone"
              label="Phone Number"
              size="small"
              name="address.phone"
              value={formik.values.address?.phone}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.phone && Boolean(formik.errors.address?.phone)}
              helperText={formik.touched.address?.phone && formik.errors.address?.phone}
              sx={{ mb: 2, maxWidth: 340 }}
            />

            <TextField
              fullWidth
              id="country"
              label="Country"
              size="small"
              name="address.country"
              value={formik.values.address?.country}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.country && Boolean(formik.errors.address?.country)}
              helperText={formik.touched.address?.country && formik.errors.address?.country}
              sx={{ mb: 1, maxWidth: 340 }}
            />
          </Grid>

          <Grid item sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}>
            <TextField
              fullWidth
              id="company"
              name="address.company"
              label="Company"
              size="small"
              value={formik.values.address?.company}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.company && Boolean(formik.errors.address?.company)}
              helperText={formik.touched.address?.company && formik.errors.address?.company}
              sx={{ mb: 2, maxWidth: 340 }}
            />

            <TextField
              fullWidth
              id="zip"
              label="Zip Code"
              size="small"
              name="address.zip"
              value={formik.values.address?.zip}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.address?.zip && Boolean(formik.errors.address?.zip)}
              helperText={formik.touched.address?.zip && formik.errors.address?.zip}
              sx={{ mb: 2, maxWidth: 340 }}
            />
          </Grid>
        </Grid>
      </Paper>

      <PricingOverview cart={cart} />
    </Container>
  )
}

export default Shipping
