import {
  Divider,
  Paper,
  TextField,
  Grid,
  Container,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
} from '@mui/material'
import { useEffect, useState } from 'react'
import { PricingOverview } from 'components'
import { useCart } from 'core'

const Payment = ({ formik }) => {
  const [paymentMethod, setPaymentMethod] = useState('card')
  const { cart } = useCart()

  const handlePaymentMethodChange = (event) => {
    setPaymentMethod(event.target.value)
  }

  useEffect(() => {
    formik.setFieldValue('payment.type', paymentMethod)
  }, [paymentMethod])

  return (
    <Container disableGutters sx={{ display: 'flex', mb: 2, gap: 2 }}>
      <Paper sx={{ p: 2, maxWidth: 783, width: '100%' }}>
        <form onSubmit={formik.handleSubmit}>
          <Grid container>
            <FormControl sx={{ width: '100%' }}>
              <RadioGroup value={paymentMethod} onChange={handlePaymentMethodChange}>
                <FormControlLabel value="card" control={<Radio />} label="Pay with Credit Card" />
                <Divider sx={{ my: 2 }} />

                {paymentMethod === 'card' && (
                  <>
                    <Grid
                      item
                      sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}
                    >
                      <TextField
                        fullWidth
                        size="small"
                        label="Name on card"
                        name="payment.card.name"
                        value={formik.values.payment?.card?.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.payment?.card?.name && Boolean(formik.errors.payment?.card?.name)}
                        helperText={formik.touched.payment?.card?.name && formik.errors.payment?.card?.name}
                        sx={{ mb: 2, maxWidth: 340 }}
                      />

                      <TextField
                        fullWidth
                        size="small"
                        label="Card Number"
                        name="payment.card.number"
                        value={formik.values.payment?.card?.number}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.payment?.card?.number && Boolean(formik.errors.payment?.card?.number)}
                        helperText={formik.touched.payment?.card?.number && formik.errors.payment?.card?.number}
                        sx={{ mb: 1, maxWidth: 340 }}
                      />
                    </Grid>
                    <Grid
                      item
                      sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}
                    >
                      <TextField
                        fullWidth
                        size="small"
                        label="Expiry date"
                        name="payment.card.expiryDate"
                        value={formik.values.payment?.card?.expiryDate}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={
                          formik.touched.payment?.card?.expiryDate && Boolean(formik.errors.payment?.card?.expiryDate)
                        }
                        helperText={formik.touched.payment?.card?.expiryDate && formik.errors.payment?.card?.expiryDate}
                        sx={{ mb: 1, maxWidth: 340 }}
                      />

                      <TextField
                        fullWidth
                        size="small"
                        label="CCV"
                        name="payment.card.ccv"
                        value={formik.values.payment?.card?.ccv}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.payment?.card?.ccv && Boolean(formik.errors.payment?.card?.ccv)}
                        helperText={formik.touched.payment?.card?.ccv && formik.errors.payment?.card?.ccv}
                        sx={{ mb: 1, maxWidth: 340 }}
                      />
                    </Grid>
                    <Divider sx={{ my: 2 }} />
                  </>
                )}
                <FormControlLabel value="paypal" control={<Radio />} label="Pay with Paypal" />
                <Divider sx={{ my: 2 }} />

                {paymentMethod === 'paypal' && (
                  <>
                    <Grid
                      item
                      sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', alignContent: 'center' }}
                    >
                      <TextField
                        fullWidth
                        id="email"
                        label="Paypal Email"
                        name="email"
                        size="small"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                        sx={{ mb: 1, maxWidth: 340 }}
                      />
                    </Grid>
                    <Divider sx={{ my: 2 }} />
                  </>
                )}

                <FormControlLabel value="delivery" control={<Radio />} label="Cash on Delivery" />
                <Divider sx={{ my: 2 }} />
              </RadioGroup>
              <TextField
                sx={{ mt: 2 }}
                fullWidth
                multiline
                minRows={3}
                label="Additional details"
                name="payment.details"
                value={formik.values.payment?.details}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.payment?.details && Boolean(formik.errors.payment?.details)}
                helperText={formik.touched.payment?.details && formik.errors.payment?.details}
              />
            </FormControl>
          </Grid>
        </form>
      </Paper>
      <PricingOverview cart={cart} />
    </Container>
  )
}

export default Payment
