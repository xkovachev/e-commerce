import { Container, Box, Typography, Paper } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import { useParams } from 'react-router-dom'
import useSWR from 'swr'
import { useCart } from 'core'
import { DisplayCurrency, PricingOverview } from 'components'
import theme from 'theme'
import { Link } from 'react-router-dom'

const OrderDetails = () => {
  const { getFinalPrice } = useCart()
  const { orderId } = useParams()
  const { data, error } = useSWR(`/checkouts/${orderId}`)
  const formattedDate = new Date(data?.createdAt).toLocaleDateString('en-GB', {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
  })

  return (
    <PageLayout error={error} data={data} container title="Order details">
      <Container disableGutters>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            bgcolor: theme.palette.grey[300],
            minHeight: 40,
            px: 1,
          }}
        >
          <Box>
            <Typography>Order ID: {orderId}</Typography>
          </Box>
          <Box>
            <Typography>Placed on: {formattedDate} </Typography>
          </Box>
          <Box>
            <Typography>Delivery status: In progress</Typography>
          </Box>
        </Box>
        <Paper sx={{ p: 1, display: 'flex', flexDirection: 'column', gap: 3 }}>
          {data?.cart?.map((item) => {
            return (
              <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Box sx={{ display: 'flex', width: '25%' }}>
                  <Link to={`/products/${item.product._id}`}>
                    <Box component="img" src={item.product.thumbnail} sx={{ width: 64, height: 64 }} />
                  </Link>
                  <Link to={`/products/${item.product._id}`} style={{ textDecoration: 'none', color: 'black' }}>
                    <Box sx={{ ml: 2 }}>
                      <Typography>{item.product.title}</Typography>
                      <Typography sx={{ color: theme.palette.grey[500], fontSize: 13 }}>
                        <DisplayCurrency number={getFinalPrice(item.product)} />
                        &nbsp;x&nbsp;{item.quantity}
                      </Typography>
                    </Box>
                  </Link>
                </Box>
                <Box sx={{ color: theme.palette.grey[500], width: '20%' }}>
                  <Typography>Product properties: Black, L</Typography>
                </Box>
                <Box sx={{ color: theme.palette.secondary.alternate, width: '15%', ':hover': { cursor: 'pointer' } }}>
                  <Typography>Write a Review</Typography>
                </Box>
              </Box>
            )
          })}
        </Paper>
        <Box sx={{ my: 2, display: 'flex', justifyContent: 'space-between' }}>
          <Box sx={{ width: '100%' }}>
            <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Typography sx={{ fontSize: 14, fontWeight: 'bold' }}>Shipping Address</Typography>
              <Typography sx={{ fontSize: 14 }}>
                {data?.address?.fullName}, {data?.address?.address1},{' '}
                {data?.address.address2 ? `${data?.address?.address2}, ` : ''}
                {data?.address?.zip}, {data?.address?.country}
              </Typography>
            </Paper>
          </Box>
          <Box sx={{ width: '100%', display: 'flex', justifyContent: 'end' }}>
            <PricingOverview cart={data?.cart} paymentMethod={data?.payment.type} />
          </Box>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default OrderDetails
