const NoPermission = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: 245 }}>
      <div>You have no permission to be on this page!</div>
    </div>
  )
}

export default NoPermission
