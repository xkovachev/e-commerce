import { Box, Card, CardContent, CardMedia, IconButton, Typography, ButtonBase } from '@mui/material'
import { DisplayCurrency } from 'components'
import { useCart } from 'core'
import { Close } from '@mui/icons-material'
import { Link } from 'react-router-dom'
import theme from 'theme'

const CartProduct = ({ product }) => {
  const { addToCart, removeFromCart, getFinalPrice, removeWholeProductFromCart, cart } = useCart()
  const finalPrice = getFinalPrice(product)

  return (
    <Card sx={{ display: 'flex', height: 140, mb: 3, position: 'relative', width: '100%', borderRadius: 2 }}>
      <Link to={`/products/${product._id}`}>
        <CardMedia component="img" sx={{ width: 140, height: '100%' }} image={product.thumbnail} alt={product.title} />
      </Link>
      <CardContent sx={{ flex: '1 0 auto', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
        <Typography variant="h6">{product.title}</Typography>
        <Box sx={{ display: 'flex' }}>
          <Typography sx={{ color: theme.palette.grey[500], fontSize: 14 }}>
            <DisplayCurrency number={finalPrice} /> x {product.quantity} =&nbsp;
          </Typography>
          <Typography sx={{ color: theme.palette.secondary.alternate, fontWeight: 600, fontSize: 14 }}>
            <DisplayCurrency number={finalPrice * product.quantity} />
          </Typography>
        </Box>
        <Box display="flex" alignItems="center" sx={{ gap: 1 }}>
          <ButtonBase
            sx={{
              color: theme.palette.secondary.alternate,
              border: `1px solid ${theme.palette.secondary.alternate}`,
              borderRadius: 1,
              width: 32,
              height: 32,
              fontSize: 20,
            }}
            onClick={() => removeFromCart(product._id)}
          >
            -
          </ButtonBase>
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Typography textAlign="center" fontWeight={600} fontSize={18}>
              {cart.map((item) => {
                if (item._id === product._id) {
                  return item.quantity
                }
              })}
            </Typography>
          </Box>
          <ButtonBase
            sx={{
              color: theme.palette.secondary.alternate,
              border: `1px solid ${theme.palette.secondary.alternate}`,
              borderRadius: 1,
              width: 32,
              height: 32,
              fontSize: 20,
            }}
            onClick={() => addToCart(product)}
          >
            +
          </ButtonBase>
        </Box>
      </CardContent>
      <IconButton
        size="small"
        sx={{ position: 'absolute', top: 0, right: 0 }}
        onClick={() => removeWholeProductFromCart(product._id)}
      >
        <Close />
      </IconButton>
    </Card>
  )
}

export default CartProduct
