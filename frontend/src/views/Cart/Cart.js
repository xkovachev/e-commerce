import { useCart } from 'core'
import { PageLayout } from 'layouts/Main/components'
import { CartProduct } from './components'
import { Button, Divider, Paper, Typography, Container, Box, TextField, ButtonBase } from '@mui/material'
import { DisplayCurrency } from 'components'
import { Link } from 'react-router-dom'
import theme from 'theme'

const Cart = () => {
  const { cart, totalPrice } = useCart()

  const centeredFlexbox = { display: 'flex', justifyContent: 'center', alignContent: 'center' }

  return (
    <PageLayout title="Cart" isAsync={false}>
      <Container disableGutters sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Box sx={{ width: '100%', mr: 3 }}>
          {cart.map((product) => (
            <CartProduct key={product._id} product={product} />
          ))}
        </Box>

        <Box>
          <Paper
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              width: 390,
              maxHeight: 736,
              flex: 1,
              p: 3,
              borderRadius: 2,
            }}
          >
            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Box>
                <Typography sx={{ fontSize: 14, color: theme.palette.grey[500] }}>Total:</Typography>
              </Box>
              <Box>
                <Typography sx={{ textAlign: 'center', p: 1, fontSize: 16, fontWeight: 600 }}>
                  <DisplayCurrency number={totalPrice} />
                </Typography>
              </Box>
            </Box>
            <Box sx={{ ...centeredFlexbox }}>
              <Divider sx={{ width: '100%' }} />
            </Box>
            <Box sx={{ ...centeredFlexbox, flexDirection: 'column' }}>
              <Box sx={{ display: 'flex', my: 2 }}>
                <Typography fontSize={14} fontWeight={600}>
                  Additional Comments &nbsp;
                </Typography>
                <Box
                  sx={{
                    backgroundColor: '#fce9ec',
                    color: theme.palette.secondary.alternate,
                    borderRadius: 1,
                    p: 0.25,
                    fontSize: 12,
                    textAlign: 'center',
                  }}
                >
                  Note
                </Box>
              </Box>

              <Box>
                <TextField multiline minRows={5} maxRows={5} name="requests" fullWidth />
              </Box>
            </Box>
            <Box sx={{ ...centeredFlexbox, my: 2 }}>
              <Divider sx={{ width: '100%' }} />
            </Box>
            <Box>
              <Box sx={{ ...centeredFlexbox, m: 1 }}>
                <TextField label="Voucher" name="voucher" size="small" fullWidth />
              </Box>
              <Box sx={{ ...centeredFlexbox }}>
                <ButtonBase
                  sx={{
                    width: '100%',
                    maxWidth: 326,
                    border: `0.5px solid ${theme.palette.secondary.alternate}`,
                    color: theme.palette.secondary.alternate,
                    p: 1,
                    borderRadius: 1,
                  }}
                >
                  <Typography fontWeight={600} fontSize={14}>
                    Apply Voucher
                  </Typography>
                </ButtonBase>
              </Box>
            </Box>
            {/* <Box sx={{ ...centeredFlexbox, my: 3 }}>
              <Divider sx={{ width: '100%' }} />
            </Box> */}

            {/* <Box sx={{ ...centeredFlexbox, flexDirection: 'column' }}>
              <Box sx={{ maxWidth: 326 }}>
                <Button
                  sx={{
                    bgcolor: theme.palette.secondary.alternate,
                    maxHeight: 40,
                    fontSize: 14,
                    color: 'white',
                    width: '100%',
                    height: 40,
                    ml: 1,
                    ':hover': {
                      bgcolor: '#dc324e',
                    },
                  }}
                >
                  <Link to="/checkout" style={{ textDecoration: 'none', color: 'white' }}>
                    <Typography fontWeight={600} fontSize={14}>
                      Checkout Now
                    </Typography>
                  </Link>
                </Button>
              </Box>
            </Box> */}

            <Box sx={{ ...centeredFlexbox }}></Box>
          </Paper>
        </Box>
      </Container>
    </PageLayout>
  )
}

export default Cart
