import { GenericProductCard } from 'views/Home/components'
import { Container, Typography, Box, Paper, CardMedia } from '@mui/material'

const Cards = ({ products }) => {
  const SmallCard = ({ imageSrc, storeName }) => {
    return (
      <Paper
        sx={{
          width: 178,
          height: 138,
          p: 2,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
          borderRadius: 2,
          ':hover': { cursor: 'pointer' },
        }}
      >
        <CardMedia component="img" src={imageSrc} sx={{ width: 48, height: 48 }} />
        <Typography fontWeight={600} sx={{ mt: 2 }}>
          {storeName}
        </Typography>
      </Paper>
    )
  }

  return (
    <Container disableGutters sx={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column', my: 5 }}>
      <Box>
        <Box>
          <Typography variant="h6" fontWeight={600}>
            Also Available at
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', gap: 4, my: 5 }}>
          <SmallCard storeName="Tech Friend" imageSrc="https://bazaar.ui-lib.com/assets/images/faces/propic.png" />
          <SmallCard storeName="Smart Shop" imageSrc="https://bazaar.ui-lib.com/assets/images/faces/propic(1).png" />
          <SmallCard storeName="Gadget 360" imageSrc="https://bazaar.ui-lib.com/assets/images/faces/propic(8).png" />
        </Box>
      </Box>

      <Box>
        <Box sx={{ mb: 5 }}>
          <Typography variant="h6" fontWeight={600}>
            Probably Not Related Products
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          {products?.map((product) => (
            <GenericProductCard product={product} />
          ))}
        </Box>
      </Box>
    </Container>
  )
}

export default Cards
