import {
  Container,
  Typography,
  Box,
  CardMedia,
  Paper,
  Grid,
  Rating,
  Button,
  ButtonBase,
  Tab,
  Tabs,
} from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import { useParams } from 'react-router-dom'
import useSWR from 'swr'
import theme from 'theme'
import { DisplayCurrency } from 'components'
import { useState } from 'react'
import { useCart } from 'core'
import DescriptionAndReviews from '../DescriptionAndReviews'
import Cards from '../Cards'

const MainDetails = () => {
  const params = useParams()
  const { getFinalPrice, addToCart, cart, removeFromCart } = useCart()

  const { data: product, error } = useSWR(`/products/${params.productId}`)
  const { data: allProducts, potentialError } = useSWR(`/products?page=1&productsPerPage=30`)

  const randomProducts = allProducts?.result?.slice(0, 4)

  const [mainImage, setMainImage] = useState(0)
  const changeImageHandler = (event, index) => {
    setMainImage(index)
  }

  return (
    <PageLayout error={error} data={product}>
      {product && (
        <>
          <Container
            disableGutters
            sx={{
              minHeight: 500,
            }}
          >
            <Box
              sx={{
                display: 'flex',
              }}
            >
              <Box
                sx={{
                  width: '50%',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'column',
                }}
              >
                <CardMedia component="img" src={product.images[mainImage]} sx={{ width: 300, height: 300 }}></CardMedia>
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                  {product.images.map((image, index) => (
                    <Paper
                      onClick={(event) => changeImageHandler(event, index)}
                      sx={{
                        m: 2,
                        border: `1px solid ${
                          mainImage === index ? theme.palette.secondary.alternate : theme.palette.grey[400]
                        }`,
                        p: 2,
                        borderRadius: 2,
                        ':hover': {
                          cursor: 'pointer',
                        },
                      }}
                    >
                      <CardMedia component="img" src={image} sx={{ width: 40, height: 40 }} />
                    </Paper>
                  ))}
                </Box>
              </Box>
              <Box>
                <Grid container sx={{ display: 'flex', flexDirection: 'column', gap: 2 }}>
                  <Grid item>
                    <Typography variant="h4">{product.title}</Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle">
                      Brand: &nbsp;<b>{product.brand}</b>
                    </Typography>
                  </Grid>
                  <Grid item sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
                    <Typography variant="subtitle">Rated:</Typography>
                    <Rating value={product.rating} readOnly size="small" />
                    <Typography variant="subtitle">(50)</Typography>
                  </Grid>
                  <Grid item sx={{ display: 'flex', flexDirection: 'column' }}>
                    <Typography fontWeight={900} color={theme.palette.secondary.alternate} fontSize={25}>
                      <DisplayCurrency number={getFinalPrice(product)} />
                    </Typography>
                    <Typography variant="subtitle2">
                      {product.stock > 0 ? 'Stock available' : 'Out of Stock'}
                    </Typography>
                  </Grid>
                  <Grid item sx={{ display: 'flex', gap: 2 }}>
                    {cart.find((item) => item._id === product._id) ? (
                      <>
                        <ButtonBase
                          sx={{
                            color: theme.palette.secondary.alternate,
                            border: `1px solid ${theme.palette.secondary.alternate}`,
                            borderRadius: 1,
                            width: 38,
                            height: 38,
                            fontSize: 20,
                          }}
                          onClick={() => removeFromCart(product._id)}
                        >
                          -
                        </ButtonBase>
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                          <Typography textAlign="center" fontWeight={600} fontSize={20}>
                            {cart.map((item) => {
                              if (item._id === product._id) {
                                return item.quantity < 10 ? `0${item.quantity}` : item.quantity
                              }
                            })}
                          </Typography>
                        </Box>
                        <ButtonBase
                          sx={{
                            color: theme.palette.secondary.alternate,
                            border: `1px solid ${theme.palette.secondary.alternate}`,
                            borderRadius: 1,
                            width: 38,
                            height: 38,
                            fontSize: 20,
                          }}
                          onClick={() => addToCart(product)}
                        >
                          +
                        </ButtonBase>
                      </>
                    ) : (
                      <Button
                        onClick={() => addToCart(product)}
                        sx={{
                          bgcolor: theme.palette.secondary.alternate,
                          maxWidth: 135,
                          maxHeight: 40,
                          fontSize: 14,
                          color: 'white',
                          width: '100%',
                          height: 40,
                          ':hover': {
                            bgcolor: '#dc324e',
                          },
                        }}
                      >
                        Add To Cart
                      </Button>
                    )}
                  </Grid>
                  <Grid item sx={{ display: 'flex', flexDirection: 'column', mt: 2 }}>
                    <Typography variant="subtitle2">
                      Sold by: &nbsp;<b>Some Legit Store Probably</b>
                    </Typography>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
          <DescriptionAndReviews product={product} />
          <Cards products={randomProducts} />
        </>
      )}
    </PageLayout>
  )
}

export default MainDetails
