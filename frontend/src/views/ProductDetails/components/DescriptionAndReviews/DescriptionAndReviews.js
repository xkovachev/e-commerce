import { styled, Box, Typography, CardMedia, TextField, Tabs, Tab, Rating, Button } from '@mui/material'
import { useState } from 'react'
import theme from 'theme'
import { DUMMY_REVIEWS } from 'utils/dummy-data/dummyReviews'

const DescriptionAndReviews = ({ product }) => {
  const [currentTab, setCurrentTab] = useState(0)

  const handleTabChange = (event, newTab) => {
    setCurrentTab(newTab)
  }

  const TabPanel = ({ children, value, index }) => {
    return <div>{value === index && <>{children}</>}</div>
  }

  return (
    <Box sx={{ width: '100%' }}>
      <Box>
        <Tabs
          TabIndicatorProps={{ style: { background: theme.palette.secondary.alternate } }}
          value={currentTab}
          onChange={handleTabChange}
          sx={{ borderBottom: 1, borderColor: theme.palette.grey[300] }}
        >
          <Tab label="Description" />
          <Tab label="Reviews (3)" />
        </Tabs>
        <TabPanel value={currentTab} index={0}>
          <Box>
            <Typography fontSize={22} fontWeight={600} sx={{ my: 3 }}>
              Specification:
            </Typography>
            <Typography fontSize={15}>Brand: {product.brand}</Typography>
            <Typography fontSize={15}>Model: It's a nice model, trust me</Typography>
            <Typography fontSize={15}>{product.description}</Typography>
            <Typography fontSize={15}>Features: Tons of cool features, no cap</Typography>
            <Typography fontSize={15}>Made in China</Typography>
          </Box>
        </TabPanel>
        <TabPanel value={currentTab} index={1}>
          {DUMMY_REVIEWS.map((review, index) => {
            return (
              <Box
                key={review.author}
                sx={{
                  gap: 2,
                  display: 'flex',
                  flexDirection: 'column',
                  my: 2,
                  width: '50%',
                }}
              >
                <Box sx={{ display: 'flex', gap: 2 }}>
                  <Box>
                    <CardMedia component="img" src={review.image} sx={{ width: 48, height: 48 }} />
                  </Box>
                  <Box>
                    <Typography fontWeight={600}>{review.author}</Typography>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 1 }}>
                      <Rating value={review.rating} size="small" readOnly />
                      <Typography fontWeight={600} fontSize={14}>
                        {review.rating}
                      </Typography>
                      <Typography fontSize={14}>{review.when}</Typography>
                    </Box>
                  </Box>
                </Box>
                <Box>
                  <Typography fontSize={14} color={theme.palette.grey[600]}>
                    {review.comment}
                  </Typography>
                </Box>
              </Box>
            )
          })}
          <Box sx={{ mt: 8 }}>
            <Box>
              <Typography variant="h5" fontWeight={600}>
                Write a Review for this product
              </Typography>
            </Box>
            <Box sx={{ mt: 3 }}>
              <Box sx={{ display: 'flex' }}>
                <Typography fontWeight={600}>Your Rating &nbsp;</Typography>
                <Typography fontWeight={600} color={theme.palette.secondary.alternate}>
                  *
                </Typography>
              </Box>
              <Box sx={{ mt: 1 }}>
                <Rating />
              </Box>
            </Box>
            <Box sx={{ mt: 3 }}>
              <Box sx={{ display: 'flex' }}>
                <Typography fontWeight={600}>Your Review &nbsp;</Typography>
                <Typography fontWeight={600} color={theme.palette.secondary.alternate}>
                  *
                </Typography>
              </Box>
              <Box sx={{ mt: 1 }}>
                <TextField
                  placeholder="Write a review here..."
                  multiline
                  maxRows={8}
                  minRows={8}
                  sx={{ width: '100%', color: 'red' }}
                />
              </Box>
              <Box sx={{ mt: 2 }}>
                <Button
                  sx={{
                    bgcolor: theme.palette.secondary.alternate,
                    maxWidth: 80,
                    maxHeight: 37,
                    fontSize: 14,
                    color: 'white',
                    width: '100%',
                    height: 40,
                    ':hover': {
                      bgcolor: '#dc324e',
                    },
                  }}
                >
                  Submit
                </Button>
              </Box>
            </Box>
          </Box>
        </TabPanel>
      </Box>
    </Box>
  )
}

export default DescriptionAndReviews
