import { Navigate, useLocation } from 'react-router-dom'
import { useAuth } from 'core'
import { PageURLs } from 'Routes'

const AdminRoute = ({ children }) => {
  const { isAdmin } = useAuth()
  let location = useLocation()

  if (!isAdmin) {
    return <Navigate to={PageURLs.NoPermission} state={{ from: location }} replace />
  } else {
    return children
  }
}

export default AdminRoute
