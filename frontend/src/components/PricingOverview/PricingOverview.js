import { Box, Divider, Paper, Typography } from '@mui/material'
import { DisplayCurrency } from 'components'
import { useCart } from 'core'
import theme from 'theme'

const PricingOverview = ({ cart, paymentMethod }) => {
  const { getFinalPrice } = useCart()
  let priceWithoutDiscount
  let finalPrice

  if (!paymentMethod) {
    priceWithoutDiscount = cart
      .map((product) => +(product.price * product.quantity).toFixed(2))
      .reduce((accumulator, currentValue) => accumulator + currentValue, 0)

    finalPrice = cart.reduce(
      (accumulator, currentValue) => accumulator + getFinalPrice(currentValue) * currentValue.quantity,
      0
    )
  } else {
    priceWithoutDiscount = cart
      .map((item) => +(item.product.price * item.quantity).toFixed(2))
      .reduce((accumulator, currentValue) => accumulator + currentValue, 0)
    console.log(priceWithoutDiscount, 'priceWithout')

    finalPrice = cart.reduce(
      (accumulator, currentValue) => accumulator + getFinalPrice(currentValue.product) * currentValue.quantity,
      0
    )

    console.log(finalPrice, 'final')
  }

  return (
    <Paper sx={{ p: 4, maxWidth: 380, width: '100%' }}>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography sx={{ fontSize: 14, color: theme.palette.grey[500], mb: 1 }}>Subtotal:</Typography>
        <Typography sx={{ fontSize: 19 }}>
          <DisplayCurrency number={priceWithoutDiscount} />
        </Typography>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography sx={{ fontSize: 14, color: theme.palette.grey[500], mb: 1 }}>Shipping:</Typography>
        <Typography sx={{ fontSize: 19 }}>
          <DisplayCurrency number={0} />
        </Typography>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography sx={{ fontSize: 14, color: theme.palette.grey[500], mb: 1 }}>Tax:</Typography>
        <Typography sx={{ fontSize: 19 }}>
          <DisplayCurrency number={10} />
        </Typography>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography sx={{ fontSize: 14, color: theme.palette.grey[500], mb: 1 }}>Discount:</Typography>
        <Typography sx={{ fontSize: 19 }}>
          <DisplayCurrency number={priceWithoutDiscount - finalPrice} />
        </Typography>
      </Box>
      <Divider sx={{ my: 1 }} />
      <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <Typography sx={{ fontSize: 16, color: theme.palette.grey[500], mb: 1 }}>Final Price:</Typography>
        <Typography sx={{ fontSize: 23 }}>
          <DisplayCurrency number={finalPrice + 10} />
        </Typography>
      </Box>
      {paymentMethod && (
        <Typography sx={{ fontSize: 12, mt: 2, fontWeight: 'bold' }}>
          Payment method: {paymentMethod !== 'delivery' ? paymentMethod : 'Cash on Delivery'}
        </Typography>
      )}
    </Paper>
  )
}

export default PricingOverview
