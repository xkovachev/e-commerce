import { Box, Stepper, Step, StepButton } from '@mui/material'
import { Link, useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import theme from 'theme'

const steps = ['Cart', 'Details', 'Payment']
const links = ['/cart', '/order-details', '/payment']

export default function HorizontalNonLinearStepper() {
  const [activeStep, setActiveStep] = useState(0)

  const newStep = window.location.pathname

  useEffect(() => {
    if (newStep === '/cart') {
      setActiveStep(0)
    } else if (newStep === '/order-details') {
      setActiveStep(1)
    } else if (newStep === '/payment') {
      setActiveStep(2)
    }
  }, [newStep])

  // const handleStep = (step) => () => {
  //   setActiveStep(step)
  // }

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', my: 3 }}>
      <Box sx={{ width: '50%' }}>
        <Stepper nonLinear activeStep={activeStep}>
          {steps.map((label, index) => (
            <Step key={label}>
              <Box
                sx={{
                  width: 120,
                  height: 50,
                  borderRadius: 2,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  border: `1px solid ${theme.palette.primary.main}`,
                  bgcolor: activeStep === index ? 'white' : theme.palette.grey[200],
                }}
              >
                {/* <Link to={links[index]} style={{ textDecoration: 'none' }}> */}
                <StepButton>{label}</StepButton>
                {/* <StepButton onClick={handleStep(index)}>{label}</StepButton> */}
                {/* </Link> */}
              </Box>
            </Step>
          ))}
        </Stepper>
      </Box>
    </Box>
  )
}
